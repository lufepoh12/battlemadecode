using System.Collections;
using System.Collections.Generic;

using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;

using UnityEngine;

namespace Platformer.Gameplay
{
    /// <summary>
    public class EnemyDelete : Simulation.Event<EnemyDelete>
    {
        public EnemyInfo enemy;

        public override void Execute()
        {
            enemy.gameObject.SetActive(false);
        }
    }
}
