using Platformer.Core;
using Platformer.Mechanics;

using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerRun : Simulation.Event<PlayerCrouch>
    {
        public PlayerController player;

        public override void Execute()
        {
            player._run = true;

            player.RunTimer += Time.deltaTime;
        }
    }
}