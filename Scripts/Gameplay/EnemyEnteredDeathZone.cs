using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;

using UnityEngine;

namespace Platformer.Gameplay
{
    public class EnemyEnteredDeathZone : Simulation.Event<EnemyEnteredDeathZone>
    {
        public DeathZone deatzone;

        public override void Execute()
        {
            Simulation.Schedule<EnemyDelete>(0);
        }
    }

}

