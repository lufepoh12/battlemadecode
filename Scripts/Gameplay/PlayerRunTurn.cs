using Platformer.Core;
using Platformer.Mechanics;

using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerRunTurn : Simulation.Event<PlayerRunTurn>
    {
        public PlayerController player;

        public override void Execute()
        {
            player._runTurn = true;
            player.velocity.x = 0;
            player._runTurnVelocityX = player.DefaultSpeed + player.PlusThirdTierSpeed;

            player.RunTimer = 0;
        }
    }
}