using Platformer.Core;
using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerStopCrouch : Simulation.Event<PlayerStopCrouch>
    {
        public PlayerController player;

        public override void Execute()
        {
            player._stopCrouch = true;
        }
    }
}


