using Platformer.Core;
using Platformer.Mechanics;

namespace Platformer.Gameplay
{
    public class PlayerWallBump : Simulation.Event<PlayerWallBump>
    {
        public PlayerController player;

        public override void Execute()
        {
            player._wallPrepare = true;
            player._wallBump = true;
        }
    }
}

