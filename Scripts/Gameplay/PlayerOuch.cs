using System.Collections;
using System.Collections.Generic;

using Platformer.Core;
using Platformer.Model;
using static Platformer.Core.Simulation;

using UnityEngine;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player has died.
    /// </summary>
    /// <typeparam name="PlayerDeath"></typeparam>
    public class PlayerOuch : Simulation.Event<PlayerOuch>
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            var player = model.player;
            if (player.MyHealth.IsAlive)
            {
                player._myAnimator.SetTrigger("hurt");
                // �ӽ�����
                //player.MyHealth.Decrement();
                if (!player.MyHealth.IsAlive)
                {
                    var ev = Schedule<HealthIsZero>();
                    ev.health = player.MyHealth;
                }
                if (player.MyAudioSource && player.OuchAudio)
                    player.MyAudioSource.PlayOneShot(player.OuchAudio);
            }
        }
    }
}
