using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player is spawned after dying.
    /// </summary>
    public class PlayerSpawn : Simulation.Event<PlayerSpawn>
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            var player = model.player;
            player.MyCollider2d.enabled = true;
            player.ControlEnabled = false;
            if (player.MyAudioSource && player.RespawnAudio)
                player.MyAudioSource.PlayOneShot(player.RespawnAudio);
            player.MyHealth.Increment();
            player.Teleport(model.spawnPoint.transform.position);
            player.JumpState = PlayerController.JumpStateType.Grounded;
            player._myAnimator.SetBool("dead", false);
            model.virtualCamera.m_Follow = player.transform;
            model.virtualCamera.m_LookAt = player.transform;
            Simulation.Schedule<EnablePlayerInput>(2f);
        }
    }
}