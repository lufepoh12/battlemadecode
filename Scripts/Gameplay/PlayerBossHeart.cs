using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerBossHeart : MonoBehaviour
    {
        public GameObject HeartObj;
        public List<Heart> HeartList = new List<Heart>();

        public int maxHp;
        public int currentHp;
        public bool IsAlive => currentHp > 0;

        private void Awake()
        {

        }

        public void InitHearts(int count)
        {
            for(int i = 0; i < count; i++)
            {
                var obj = Instantiate(HeartObj, transform);
                HeartList.Add(obj.GetComponent<Heart>());
            }

            for(int i = 0;i < HeartList.Count;i++)
            {
                HeartList[i].gameObject.SetActive(true);
                if ((i + 1) < HeartList.Count)
                {
                    HeartList[i].InitActiveHeart(HeartList[i + 1]);
                }
                else
                {
                    HeartList[i].InitActiveHeart(null);
                }
            }

            currentHp = HeartList.Count;
            maxHp = HeartList.Count;
        }

        public void ClearHearts()
        {
            foreach(var heart in HeartList)
            {
                Destroy(heart.gameObject);
            }
            HeartList.Clear();
        }

        public void Decrement()
        {
            currentHp -= 1;
            if (currentHp > 0)
                ChangeHeartState(false);
        }

        public void Increment()
        {
            currentHp += 1;
            if (currentHp < maxHp)
            {
                ChangeHeartState(true);
            }
        }

        private void ChangeHeartState(bool change)
        {
            if (change)
            {
                // ����
                var heart = HeartList.FindLast(x => !x.IsActive);
                heart.SetActiveHeart();
            }
            else
            {
                // ����
                var heart = HeartList.FindLast(x => x.IsActive);
                heart.SetDisableHeart();
            }
        }
    }

}

