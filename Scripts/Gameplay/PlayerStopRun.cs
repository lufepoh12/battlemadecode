using Platformer.Core;
using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerStopRun : Simulation.Event<PlayerStopRun>
    {
        public PlayerController player;

        public override void Execute()
        {
            player._run = false;
            player._runTurn = false;
            player._stopRun = true;

            player.RunTierState = PlayerController.RunTierType.One;

            player._turnStopTimer = 0f;
            player.RunTimer = 0;
        }
    }
}