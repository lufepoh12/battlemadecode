using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    // 플레이어가 특정 영역에 들어오는지만 체크.
    public class CloseDoorPos : MonoBehaviour
    {
        public BossMapController BossMap;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            var player = collision.GetComponent<PlayerController>();
            if (player != null )
            {
                player.ControlEnabled = false;
                gameObject.SetActive(false);
                BossMap.BossMapOpening();
            }
        }
    }
}


