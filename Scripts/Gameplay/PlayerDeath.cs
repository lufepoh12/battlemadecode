﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;
using UnityEngine;

namespace Platformer.Gameplay
{
    /// <summary>
    /// Fired when the player has died.
    /// </summary>
    /// <typeparam name="PlayerDeath"></typeparam>
    public class PlayerDeath : Simulation.Event<PlayerDeath>
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public BossController bossController;
        public bool re;

        public override async void Execute()
        {
             var player = model.player;
            if (!player.MyHeart.IsAlive)
            {
                if (re)
                {
                    // 체력바 삭제
                    model.player.MyHeart.ClearHearts();
                    // 보스 삭제
                    BossController.IsOne = false;
                    bossController = null;
                    // GameOver 이미지
                    await model.cutSceneManager.OnCutScene(CutSceneManager.CutSceneType.GameOver);
                    await Task.Delay(2000);

                    // 다시시작
                    model.loadController.LoadActionAsyncLogic
                        (new Action(() =>
                        {
                            model.mapController.CurrentMusicName = "";
                            model.mapController.OpenMap(Convert.ToByte(model.mapController.CurrentMap));

                            model.player.ControlEnabled = true;
                            model.cutSceneManager.CutSceneReset(CutSceneManager.CutSceneType.GameOver);
                        }), 0.2f);
                }
                else
                {
                    player.MyHealth.Die();
                    model.virtualCamera.m_Follow = null;
                    model.virtualCamera.m_LookAt = null;
                    // player.collider.enabled = false;
                    player.ControlEnabled = false;

                    if (player.MyAudioSource && player.RespawnAudio)
                        player.MyAudioSource.PlayOneShot(player.RespawnAudio);
                    //player._myAnimator.SetTrigger("hurt");
                    player._myAnimator.SetBool("dead", true);
                    Simulation.Schedule<PlayerSpawn>(2);
                }
            }
        }
    }
}