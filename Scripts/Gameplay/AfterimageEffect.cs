using Platformer.Mechanics;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

public class AfterimageEffect : MonoBehaviour
{
    public PlayerController Player;
    public SpriteRenderer CharacterSpriteRenderer;
    public float ThresHoldSpeed = 10f;
    public float AfterImageDuration = 0.5f;
    public float WaitTime = 0.1f;

    public Action waitDestroy = null;

    private float timeSinceLastAfterimage;

    void Update()
    {
        if (Player.CrouchState == PlayerController.CrouchStateType.Idle &&
            Player.RunState == PlayerController.RunStateType.Run &&
            Player.RunTimer >= Player.RunTierChangeRunTime)
        {
            Vector2 speed = Player.velocity;
            var normalSpeed = speed.normalized;

            if ((normalSpeed.x > 0.1f || normalSpeed.x < -0.1f) && timeSinceLastAfterimage > WaitTime)
            {
                GameObject afterimage = Instantiate(gameObject);
                //afterimage.transform.parent = this.transform;
                afterimage.GetComponent<AfterimageEffect>().enabled = false;
                afterimage.transform.position = (Vector2)transform.position - speed * Time.deltaTime;
                afterimage.GetComponent<SpriteRenderer>().sprite = CharacterSpriteRenderer.sprite;

                if (normalSpeed.x > 0f)
                    afterimage.GetComponent<SpriteRenderer>().flipX = true;
                else if (normalSpeed.x < 0f)
                    afterimage.GetComponent<SpriteRenderer>().flipX = false;

                afterimage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);

                Destroy(afterimage, AfterImageDuration);

                timeSinceLastAfterimage = 0f;
            }

            timeSinceLastAfterimage += Time.deltaTime;
        }

        if (Player.RunTierState == PlayerController.RunTierType.Three &&
            Player.WallState == PlayerController.WallStateType.Jump)
        {
            Debug.Log("ASD");
        }
    }
}