using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerPlusCollisionCheck : MonoBehaviour
    {
        public enum AllDirectionsType
        {
            Top,
            Left,
            Right,
            Bottom,
        }
        public AllDirectionsType Direction;

        public bool IsTopCheck = false;
        public bool IsLeftCheck = false;
        public bool IsRightCheck = false;
        public bool IsBottomCheck = false;

        private PlayerController parent;

        public float rayDistance = 1f;
        public Color rayColor = Color.red;
        public bool drawDebugRay = true;

        private void Awake()
        {
            parent = transform.parent.GetComponent<PlayerController>();
        }

        private void Update()
        {
            //if (Direction == AllDirectionsType.Bottom)
            //{
            //    // Raycast 를 생성
            //    Ray2D ray = new Ray2D(transform.position, Vector2.down);

            //    // Raycast 발사
            //    RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, rayDistance);

            //    // 디버그 뷰에서 Raycast 충돌 지점 시각화
            //    if (drawDebugRay)
            //    {
            //        Debug.DrawRay(ray.origin, ray.direction * rayDistance, rayColor);
            //    }

            //    // 충돌한 오브젝트를 로그로 출력
            //    if (hit.collider != null)
            //    {
            //        Debug.Log("Raycast hit object: " + hit.collider.gameObject.name);

            //        var colCheck = hit.collider.gameObject.GetComponent<WallController>();
            //        if (colCheck != null)
            //        {
            //            GetComponent<BoxCollider2D>().isTrigger = false;
            //        }
            //        else
            //        {
            //            GetComponent<BoxCollider2D>().isTrigger = true;
            //        }
            //    }
            //}
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == 7)
            {
                switch (Direction)
                {
                    case AllDirectionsType.Top:
                        IsTopCheck = true;
                        break;
                    case AllDirectionsType.Left:
                        break;
                    case AllDirectionsType.Right:
                        break;
                    case AllDirectionsType.Bottom:
                        IsBottomCheck = false;
                        break;
                }
            }
            else if (collision.gameObject.layer == 9)
            {
                if (Direction == AllDirectionsType.Bottom)
                {
                    GetComponent<BoxCollider2D>().isTrigger = false;
                    IsBottomCheck = true;
                }
            }
            else
            {
                IsTopCheck = false;
                IsBottomCheck = false;
            }
        }
        public void OnTriggerStay2D(Collider2D collision)
        {
            //Debug.Log("Stay : " +  collision.gameObject.name + " | " + collision.gameObject.layer);
            if (collision.gameObject.layer == 7)
            {
                switch (Direction)
                {
                    case AllDirectionsType.Top:
                        IsTopCheck = true;
                        break;
                    case AllDirectionsType.Left:
                        break;
                    case AllDirectionsType.Right:
                        break;
                    case AllDirectionsType.Bottom:
                        IsBottomCheck = false;
                        break;
                }
            }
            else if (collision.gameObject.layer == 9)
            {
                if (Direction == AllDirectionsType.Bottom)
                {
                    GetComponent<BoxCollider2D>().isTrigger = false;
                    IsBottomCheck = true;
                }
                
            }
            else
            {
                IsTopCheck = false;
                IsBottomCheck = false;
            }
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            Debug.Log("Stay : " + collision.gameObject.name);

            if (collision.gameObject.layer == 9)
            {
                if (Direction == AllDirectionsType.Bottom)
                {
                    
                    //GetComponent<BoxCollider2D>().isTrigger = false;
                    IsBottomCheck = true;
                }

            }
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            //Debug.Log("Exit : " + collision.gameObject.name);
            if (collision.gameObject.layer == 7)
            {
                switch (Direction)
                {
                    case AllDirectionsType.Top:
                        IsTopCheck = false;
                        break;
                    case AllDirectionsType.Left:
                        break;
                    case AllDirectionsType.Right:
                        break;
                    case AllDirectionsType.Bottom:
                        IsBottomCheck = false;
                        break;
                }
            }
        }
    }

}