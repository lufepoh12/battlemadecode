using Platformer.Core;
using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class EnemyRadar : MonoBehaviour, DefaultDefin
    {
        public PlayerController Player;
        public bool OnRadarPlayer;
        public bool Direction = false; // false Left true right
        public float distanceThreshold = 10f;
        public float distanceThresholdY = 0.5f;

        public float Distance = 0f;

        public void Init()
        {
            Player = FindAnyObjectByType<PlayerController>();
        }

        public void Awake()
        {
            Init();
        }

        private void Update()
        {
            if (Player != null)
            {
                // 두 오브젝트 간의 거리 계산
                //Distance = Mathf.Abs(Player.transform.position.x - transform.position.x);


                // 거리가 일정 범위 이내인지 체크
                //if (Distance < distanceThreshold)
                //{
                //    // 거리가 일정 범위 이내일 때의 동작
                //    OnRadarPlayer = true;
                //    if (Player.transform.position.x < transform.position.x)
                //        Direction = false;
                //    else
                //        Direction = true;
                //}
                //else
                //{
                //    OnRadarPlayer = false;
                //}

                if (Player.transform.position.x < transform.position.x)
                    Direction = false;
                else
                    Direction = true;

                if (Player.RunTierState >= PlayerController.RunTierType.Three)
                {
                    // 두 오브젝트 간의 거리 계산
                    double deltaX = Mathf.Abs(Player.transform.position.x - transform.position.x);
                    double deltaY = Mathf.Abs(Player.transform.position.y - transform.position.y);

                    if (deltaX < distanceThreshold && deltaY < distanceThresholdY)
                    {
                        // 거리가 일정 범위 이내일 때의 동작
                        OnRadarPlayer = true;
                        //if (Player.transform.position.x < transform.position.x)
                        //    Direction = false;
                        //else
                        //    Direction = true;
                    }
                    else
                    {
                        OnRadarPlayer = false;
                    }
                }
                else
                {
                    OnRadarPlayer = false;
                }
            }
            else
            {
                Player = FindAnyObjectByType<PlayerController>();
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, distanceThreshold);
        }
    }
}