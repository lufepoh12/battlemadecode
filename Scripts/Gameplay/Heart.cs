using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Platformer.Gameplay
{
    public class Heart : MonoBehaviour
    {
        public bool IsActive = false;

        public Sprite ActiveHeart;
        public Sprite DisabledHeart;
        public Heart NextHeart;
        public Image MyImage; 

        private void Awake()
        {
            MyImage = GetComponent<Image>();
        }

        public void InitActiveHeart(Heart next)
        {
            SetActiveHeart();
            if (next != null )
            {
                NextHeart = next;
            }
        }

        public void SetActiveHeart()
        {
            IsActive = true;
            MyImage.sprite = ActiveHeart;
        }

        public void SetDisableHeart()
        {
            IsActive = false;
            MyImage.sprite = DisabledHeart;
        }
    }
}


