using System.Runtime.InteropServices;

using UnityEngine;

namespace Platformer.Gameplay
{
    /// <summary>
    /// 몬스터인지 구분하기 만듬
    /// </summary>
    public interface EnemyInfo
    {
        public enum EnemyState
        {
            LIVE = 0,
            DIE = 1,
        }

        public enum EnemyKindType
        {
            Neutral, // 공격 안하는 몬스터
            FrontAttack, // 보는 방향으로 공격하는 몬스터
            Attack, // 공격하는 몬스터
            Boss
        }

        public EnemyState State { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public GameObject gameObject { get; }
    }

}