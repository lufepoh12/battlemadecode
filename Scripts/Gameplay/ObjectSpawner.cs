using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static UnityEngine.Rendering.DebugUI;

namespace Platformer.Gameplay
{
    public class ObjectSpawner : MonoBehaviour
    {
        public float minX;
        public float maxX;
        public float minY;
        public float maxY;
        public float downSpeed;

        public GameObject SpawnObject(GameObject spawnGo)
        {
            // Generate a random position within the specified range
            // Create the object at the random position and the specified spawn height
            Vector3 spawnPosition = GetRandomSpawnerPos();
            var go = Instantiate(spawnGo, spawnPosition, Quaternion.identity);
            //spawnGo.transform.position = spawnPosition;

            Destroy(go, 5f);

            // Apply gravity by adding a Rigidbody component to the instantiated object
            //var rb = spawnGo.GetComponent<Rigidbody2D>();
            //rb.useGravity = true;

            return go;
        }

        /// <summary>
        /// 스포너에 설정된 포지션으로 랜덤된 값을 리턴.
        /// </summary>
        /// <returns></returns>
        public Vector2 GetRandomSpawnerPos()
        {
            // Generate a random position within the specified range
            float x = Random.Range(minX, maxX);
            float y = Random.Range(minY, maxY);

            return new Vector2(x, y);
        }

        /// <summary>
        /// Y 축의 평균을 구함.
        /// </summary>
        /// <returns></returns>
        public float GetSpawnerAverageY()
        {
            return (minY + maxY) / 2;
        }

        void OnDrawGizmosSelected()
        {
            // Draws a blue line from this transform to the target
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(new Vector2(minX, minY), new Vector2(maxX, maxY));
        }
    }

}
