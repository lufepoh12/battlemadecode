using Platformer.Core;
using Platformer.Mechanics;

using UnityEngine;

namespace Platformer.Gameplay
{
    public class EnemyBoardDeath : Simulation.Event<EnemyBoardDeath>
    {
        public EnemyBoardController enemy;

        public override void Execute()
        {
            enemy._collider.enabled = false;
            enemy.State = EnemyInfo.EnemyState.DIE;
            if (enemy._audio && enemy.Ouch)
            {
                enemy._audio.PlayOneShot(enemy.Ouch);
            }
        }
    }
}