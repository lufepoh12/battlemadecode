using Platformer.Core;
using Platformer.Mechanics;

namespace Platformer.Gameplay
{
    public class PlayerWallOut : Simulation.Event<PlayerWallOut>
    {
        public PlayerController player;

        public override void Execute()
        {
            player.WallState = PlayerController.WallStateType.Idle;
        }
    }
}
