using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;

using UnityEngine;

using static Platformer.Core.Simulation;

namespace Platformer.Gameplay
{

    /// <summary>
    /// Fired when a Player collides with an Enemy.
    /// </summary>
    /// <typeparam name="EnemyCollision"></typeparam>
    public class BulletEnemayCollision : Simulation.Event<BulletEnemayCollision>
    {
        public EnemyController enemy;
        // 가만히 서있는 몬스터 처리
        public EnemyBoardController enemyBoard;
        public byte gunDamage;

        public bool HitBullet = false;

        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            // 움직이는 몬스터 처리?
            if (enemy != null)
            {
                if (HitBullet)
                {
                    HitBullet = false;

                    var enemyHealth = enemy.GetComponent<Health>();
                    if (enemyHealth != null)
                    {
                        enemyHealth.Decrement(gunDamage);
                        if (!enemyHealth.IsAlive)
                        {
                            Schedule<EnemyDeath>().enemy = enemy;
                        }
                    }
                }
            }
            else if (enemyBoard != null) // 고정 몬스터
            {
                // 고정 몬스터는 무조건 죽음.
                Schedule<EnemyBoardDeath>().enemy = enemyBoard;
            }

        }
    }
}