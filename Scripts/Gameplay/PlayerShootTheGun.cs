using Platformer.Core;
using Platformer.Mechanics;

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class PlayerShootTheGun : MonoBehaviour, DefaultDefin
    {
        public enum GunType
        {
            Idle,
            Pistol,
        }
        [Serializable]
        public class GunData
        {
            public GunData(GunType gunType, float damage, float reload, float bulletSpeed)
            {
                GunType = gunType;
                Damage = damage;
                Reload = reload;
                BulletSpeed = bulletSpeed;
            }
            public GunData()
            {
                
            }
            public GunType GunType;
            public float Damage;
            public float Reload;
            public float BulletSpeed;
        }
        [SerializeField]
        public List<GunData> GunDataList;

        public PlayerController Player;
        public GameObject BulletPrefab;
        public Transform LeftFirePoint;
        public Transform RightFirePoint;
        public GunData CurrentWeapon = null;

        private float reloadTime = 0f;

        internal bool ReadyToShoot = true;

        public void Init()
        {
            Player = GetComponent<PlayerController>();

            // 총 데이터 선언
            //GunDataList = new List<GunData>();
            //var pistol = new GunData();
            //pistol.GunType = GunType.Pistol;
            //pistol.Damage = 1f;
            //pistol.Reload = 2f;
            //pistol.BulletSpeed = 10f;

            //GunDataList.Add(pistol);

            // 임시로 무조건 권총 지급.
            CurrentWeapon = GunDataList[0];
        }

        private void Awake()
        {
            Init();
        }

        private void Update()
        {
            if (CurrentWeapon != null)
            {
                reloadTime += Time.deltaTime;

                if (CurrentWeapon.Reload < reloadTime)
                {
                    ReadyToShoot = true;
                    reloadTime = 0f;
                }
            }
        }

        private GunData FindGunData(GunType gunType)
        {
            var gun = GunDataList.Find(x => x.GunType == gunType);

            if (gun == null)
            {
                Debug.Log("존재하지 않는 총");
                return null;
            }

            return gun;
        }

        public void GetWeapon(GunType gunType)
        {
            switch(gunType)
            {
                case GunType.Pistol:
                    CurrentWeapon = FindGunData(gunType);
                    break;
            }
        }

        public void OnShoot()
        {
            if (CurrentWeapon != null && ReadyToShoot)
            {
                if (Player == null)
                {
                    return;
                }

                if (Player.PlayerDirState == PlayerController.PlayerDirType.Left)
                {
                    GameObject bullet = Instantiate(BulletPrefab, LeftFirePoint.position, LeftFirePoint.rotation);
                    bullet.GetComponent<BulletCollision>().damage = (byte)CurrentWeapon.Damage;
                    bullet.GetComponent<SpriteRenderer>().flipX = false;
                    Rigidbody2D bulletRigidbody = bullet.GetComponent<Rigidbody2D>();
                    bulletRigidbody.velocity = -LeftFirePoint.right * CurrentWeapon.BulletSpeed;
                }
                else if (Player.PlayerDirState == PlayerController.PlayerDirType.Right)
                {
                    GameObject bullet = Instantiate(BulletPrefab, RightFirePoint.position, RightFirePoint.rotation);
                    bullet.GetComponent<BulletCollision>().damage = (byte)CurrentWeapon.Damage;
                    bullet.GetComponent<SpriteRenderer>().flipX = true;
                    Rigidbody2D bulletRigidbody = bullet.GetComponent<Rigidbody2D>();
                    bulletRigidbody.velocity = RightFirePoint.right * CurrentWeapon.BulletSpeed;
                }

                ReadyToShoot = false;
            }
        }
    }

}