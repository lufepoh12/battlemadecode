using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;
using UnityEngine;
using static Platformer.Core.Simulation;

namespace Platformer.Gameplay
{

    /// <summary>
    /// Fired when a Player collides with an Enemy.
    /// </summary>
    /// <typeparam name="EnemyCollision"></typeparam>
    public class PlayerEnemyCollision : Simulation.Event<PlayerEnemyCollision>
    {
        public EnemyController enemy;
        // 가만히 서있는 몬스터 처리
        public EnemyBoardController enemyBoard;
        public PlayerController player;

        public bool HitBullet = false;

        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public override void Execute()
        {
            // 움직이는 몬스터 처리?
            if (enemy != null)
            {
                var willHurtEnemy = player.Bounds.center.y >= enemy.Bounds.max.y;

                if (willHurtEnemy || player.RunTierState >= PlayerController.RunTierType.Three)
                {
                    var enemyHealth = enemy.GetComponent<Health>();
                    if (enemyHealth != null)
                    {
                        enemyHealth.Decrement();
                        if (!enemyHealth.IsAlive)
                        {
                            Schedule<EnemyDeath>().enemy = enemy;
                            player.Bounce(2);
                        }
                        else
                        {
                            player.Bounce(7);
                        }
                    }
                    else
                    {
                        Schedule<EnemyDeath>().enemy = enemy;
                        player.Bounce(2);
                    }
                }
                else
                {
                    switch (enemy.Kind)
                    {
                        case EnemyInfo.EnemyKindType.Neutral:
                            player.Bounce(7);
                            break;

                        case EnemyInfo.EnemyKindType.FrontAttack:
                            if ((player.PlayerDirState == PlayerController.PlayerDirType.Right &&
                                !enemy.control.EnemyRadar.Direction) ||
                                (player.PlayerDirState == PlayerController.PlayerDirType.Left &&
                                enemy.control.EnemyRadar.Direction))
                            {
                                Schedule<PlayerOuch>();
                            }
                            else
                            {
                                player.Bounce(7);
                            }
                            break;
                    }
                    //// 안 죽게
                    ////Schedule<PlayerDeath>();
                    //player.Bounce(7);
                }
            }
            else if (enemyBoard != null) // 고정 몬스터
            {
                // 고정 몬스터는 무조건 죽음.
                Schedule<EnemyBoardDeath>().enemy = enemyBoard;
            }
            
        }
    }
}