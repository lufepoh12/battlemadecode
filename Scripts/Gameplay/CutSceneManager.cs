using Platformer.Core;
using Platformer.Mechanics;
using Platformer.Model;

using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class CutSceneManager : MonoBehaviour
{
    public const string CutSceneCanvasName = "CutSceneCanvas";

    PlatformerModel model = Simulation.GetModel<PlatformerModel>();
    public enum CutSceneType
    {
        BossStartCutScene,
        GameOver,
        GameWin,
        Intro,
        ElebatorUp,
    }
    public List<GameObject> Boss_CutSceneSpr;
    public List<GameObject> Intro_CutSceneSpr;
    public List<GameObject> GameWin_CutSceneSpr;

    public AudioSource MyAudioSource;
    public AudioClip DefaultCutSceneMusic;

    public GameObject BossStartCutScene;
    public GameObject ElebatorCutScene;
    public AudioClip BossStartCutSceneMusic;
    public GameObject GameOver;
    public GameObject IntroCutScene;
    public GameObject GameWin;

    public static bool CutScenePlay = false;

    public int WaitTime = 1000;

    private void Awake()
    {
        MyAudioSource = GetComponent<AudioSource>();

        BossStartCutScene.SetActive(false);
        GameOver.SetActive(false);
        IntroCutScene.SetActive(false);
        ElebatorCutScene.SetActive(false);
    }

    public async Task OnCutScene(CutSceneType sceneType)
    {
        CutScenePlay = true;
        switch (sceneType)
        {
            case CutSceneType.BossStartCutScene:
                BossStartCutScene.SetActive(true);
                OnCutSceneMusic(BossStartCutSceneMusic);
                await Task.Delay(WaitTime);
                Boss_CutSceneSpr[0].SetActive(true);
                await Task.Delay(WaitTime);
                Boss_CutSceneSpr[1].SetActive(true);
                await Task.Delay(WaitTime);
                Boss_CutSceneSpr[2].SetActive(true);
                await Task.Delay(WaitTime);
                CutSceneReset(sceneType);
                StopMusic();
                BossStartCutScene.SetActive(false);
                break;

            case CutSceneType.GameOver:
                GameOver.SetActive(true);
                FadeOut(GameOver);
                StopMusic();
                break;

            case CutSceneType.Intro:
                IntroCutScene.SetActive(true);
                OnCutSceneMusic(DefaultCutSceneMusic);
                await Task.Delay(WaitTime);
                foreach (var cut in Intro_CutSceneSpr)
                {
                    cut.SetActive(true);
                    await Task.Delay(WaitTime);
                }
                CutSceneReset(sceneType);
                StopMusic();
                IntroCutScene.SetActive(false);
                break;

            case CutSceneType.GameWin:
                GameWin.SetActive(true);
                OnCutSceneMusic(DefaultCutSceneMusic);
                await Task.Delay(WaitTime);
                foreach (var cut in GameWin_CutSceneSpr)
                {
                    cut.SetActive(true);
                    await Task.Delay(WaitTime * 2);
                }
                CutSceneReset(sceneType);
                StopMusic();
                break;

            case CutSceneType.ElebatorUp:
                ElebatorCutScene.SetActive(true);
                FadeOut(ElebatorCutScene);
                CutSceneReset(sceneType);
                break;
        }
        CutScenePlay = false;
    }

    public void CutSceneReset(CutSceneType sceneType)
    {
        switch (sceneType)
        {
            case CutSceneType.BossStartCutScene:
                foreach(var cut in  Boss_CutSceneSpr)
                {
                    cut.SetActive(false);
                }
                break;
            case CutSceneType.GameOver:
                GameOver.SetActive(false);
                GameOver.GetComponent<Image>().color = 
                    new Color(1f, 1f, 1f, 0f);
                break;
            case CutSceneType.Intro:
                foreach (var cut in Intro_CutSceneSpr)
                {
                    cut.SetActive(false);
                }
                break;
            case CutSceneType.GameWin:
                foreach (var cut in Intro_CutSceneSpr)
                {
                    cut.SetActive(false);
                }
                break;
            case CutSceneType.ElebatorUp:
                ElebatorCutScene.SetActive(false);
                GameOver.GetComponent<Image>().color =
                    new Color(1f, 1f, 1f, 0f);
                break;
        }
    }

    public void OnCutSceneMusic(AudioClip clip)
    {
        MyAudioSource.clip = clip;
        MyAudioSource.Play();
    }

    public void StopMusic()
    {
        MyAudioSource.Stop();
    }

    private async void FadeOut(GameObject obj, float speed = 1f)
    {
        float duration = speed;
        float counter = 0;
        var image = obj.GetComponent<Image>();
        Color spriteColor = image.color;

        while (counter < duration)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(0, 1, counter / duration);

            image.color = new Color(spriteColor.r, spriteColor.g, spriteColor.b, alpha);
            await Task.Yield();
        }
        image.color =
            new Color(image.color.r, image.color.g, image.color.b, 1f);
    }
}
