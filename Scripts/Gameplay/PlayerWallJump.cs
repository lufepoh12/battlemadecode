using Platformer.Core;
using Platformer.Mechanics;

namespace Platformer.Gameplay
{
    public class PlayerWallJump : Simulation.Event<PlayerWallJump>
    {
        public PlayerController player;

        public override void Execute()
        {
            player.velocity.x = player.WallJumpSpeed * -1;
            //if (player.PlayerDirState == PlayerController.PlayerDirType.Left)
            //{
            //    player.velocity.x = player.WallJumpSpeed * -1;
            //}
            //else
            //{
            //    player.velocity.x = player.WallJumpSpeed * 1;
            //}
            
        }
    }
}
