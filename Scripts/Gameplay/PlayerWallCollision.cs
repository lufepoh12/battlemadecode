using Platformer.Core;
using Platformer.Mechanics;

using System.Diagnostics;

namespace Platformer.Gameplay
{
    public class PlayerWallCollision : Simulation.Event<PlayerWallCollision>
    {
        public PlayerController player;
        public WallController.WallType wallType = WallController.WallType.IDLE;

        public override void Execute()
        {
            
            if (wallType == WallController.WallType.WALL)
            {
                // 점프 상태이면
                if (!player.IsGrounded &&
                    player.RunTierState > PlayerController.RunTierType.One)
                {
                    UnityEngine.Debug.Log("벽 올라가기 준비");
                    player.WallState = PlayerController.WallStateType.PrepareToWallWalk;
                }
                else
                {
                    UnityEngine.Debug.Log("벽 부딪힘.");
                    player.WallState = PlayerController.WallStateType.PrepareToWallBump;
                }
            }
            else if (wallType == WallController.WallType.BROKEN_WALL)
            {
                if (player.RunTierState == PlayerController.RunTierType.Three)
                {
                    UnityEngine.Debug.Log("벽 부숨");
                }
                else
                {
                    UnityEngine.Debug.Log("벽 부수기 실패");
                    player.WallState = PlayerController.WallStateType.PrepareToWallBump;
                }
            }

            //player.WallState = PlayerController.WallStateType.PrepareToWallJump;
        }
    }
}
