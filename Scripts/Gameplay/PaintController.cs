using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class PaintController : MonoBehaviour
    {
        public BossController boss;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Bullet"))
            {
                new GameObject("DesPos").transform.position = transform.position;
                //Instantiate(new GameObject("DesPos"), transform.position, Quaternion.identity);
                boss.PattenStop();
                Destroy(gameObject);
            }
        }
    }

}

