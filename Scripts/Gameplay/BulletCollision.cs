using Platformer.Core;
using Platformer.Mechanics;
using static Platformer.Core.Simulation;

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Gameplay
{
    public class BulletCollision : MonoBehaviour, DefaultDefin
    {
        public float BulletDestoryTime = 3f;

        public BoxCollider2D MyBoxCollider2D;

        internal byte damage = 0;

        public void Init()
        {
            MyBoxCollider2D = GetComponent<BoxCollider2D>();
            Destroy(gameObject, BulletDestoryTime);
        }

        private void OnEnable()
        {
            Init();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Debug.Log("�Ѿ� : " + collision.name);
            
            if (collision.GetComponent<EnemyController>() != null)
            {
                var ev = Schedule<BulletEnemayCollision>();
                ev.enemy = collision.GetComponent<EnemyController>();
                ev.gunDamage = damage;
                ev.HitBullet = true;

                Destroy(gameObject);
                return;
            }
            else if (!collision.name.Contains("CinemachineConfiner") && !collision.name.Contains("Lilpa")
                && !collision.GetComponent<DoorController>())
            {
                Destroy(gameObject);
                return;
            }
        }
    }

}

