using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;

namespace Platformer.Core
{
    public class ResourcesManager : MonoBehaviour,DefaultDefin
    {
        private static readonly object padlock = new object();
        private static ResourcesManager instance;

        private const string PREFABS_PATH = "Prefabs";
        private const string AUDIO_PATH = "Audio";

        public static ResourcesManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock(padlock)
                    {
                        instance = FindObjectOfType<ResourcesManager>();
                        if (instance == null)
                        {
                            GameObject singletonObject = new GameObject();
                            instance = singletonObject.AddComponent<ResourcesManager>();
                            singletonObject.name = typeof(ResourcesManager).ToString() + " (Singleton)";
                            DontDestroyOnLoad(singletonObject);
                        }
                    }
                }

                return instance;
            }
        }

        public void Init()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Awake()
        {
            Init();
        }

        // 경로 만들기
        private string ResPrefabLoadPath(string path)
        {
            return PREFABS_PATH + "/" + path;
        }
        private string ResAudioLoadPath(string path)
        {
            return AUDIO_PATH + "/" + path;
        }

        /// <summary>
        /// 동적으로 GameObejct 를 로드.
        /// </summary>
        /// <param name="path">UI/asds 형식으로 넣어야함.</param>
        /// <returns></returns>
        public async Task<GameObject> ResLoadGameObjectAsync(string path)
        {
            var gb = Resources.LoadAsync<GameObject>(ResPrefabLoadPath(path));

            while(!gb.isDone)
            {
                await Task.Yield();
            }

            var result = gb.asset as GameObject;

            if (result == null)
            {
                Debug.LogError(path + " 로드 실패");
                return null;
            }

            Debug.Log(result.name + " 로드 성공");
            return result;
        }

        /// <summary>
        /// 특정 경로의 모든 GameObject 를 로드.
        /// </summary>
        /// <param name="path">Prefab 경로</param>
        /// <returns>List[GameObejct]</returns>
        public List<GameObject> ResLoadGameObjectsAsync(string path)
        {
            var gbs = Resources.LoadAll<GameObject>(ResPrefabLoadPath(path));

            if (gbs.Length == 0)
            {
                Debug.LogError(path + " Prefab 이 없음");
                return null;
            }

            return gbs.ToList();
        }

        /// <summary>
        /// 오디오 소스 로드.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<AudioClip> ResLoadAudioClipAsync(string path)
        {
            var ac = Resources.LoadAsync<AudioClip>(ResAudioLoadPath(path));

            while (!ac.isDone)
            {
                await Task.Yield();
            }

            var result = ac.asset as AudioClip;

            if (result == null)
            {
                Debug.LogError(path + " 로드 실패");
                return null;
            }

            Debug.Log(result.name + " 로드 성공");
            return result;
        }
    }

}
