using Platformer.Mechanics;

using System.Collections;
using System.Collections.Generic;
using System.IO;

using UnityEngine;

namespace Platformer.Core
{
    public class JsonManager : MonoBehaviour, DefaultDefin
    {
        private static readonly object padlock = new object();
        private static JsonManager instance;

        public static JsonManager Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        instance = FindObjectOfType<JsonManager>();
                        if (instance == null)
                        {
                            GameObject singletonObject = new GameObject();
                            instance = singletonObject.AddComponent<JsonManager>();
                            singletonObject.name = typeof(JsonManager).ToString() + " (Singleton)";
                            DontDestroyOnLoad(singletonObject);
                        }
                    }
                }

                return instance;
            }
        }

        [System.Serializable]
        public class DebugSetting
        {
            public float Gravity; // �⺻ �߷�
            public float SlamGravity; // ���� �߷�
            public float DefaultSpeed;
            public float PlusSecondTierSpeed;
            public float PlusThirdTierSpeed;
            public float CrouchSpeed;
            public float SlidingBaseSpeed;
            public float SlidingDeceleration;
            public float JumpHigh;
            public float TierChangeTime;
            public float TurnBaseSpeed;
            public float TurnAcceleration;
            public float TurnStopTime;
            public float WallWalkSpeed;
            public float WallJumpSpeed;
        }
        private DebugSetting debugSet;

        private static string settingsFilePath = Application.dataPath + "/settings.json";

        public void Init()
        {

        }

        private void Awake()
        {
            
        }


        public void SaveDebugSettings(PlayerController player)
        {
            debugSet = new DebugSetting();
            debugSet.Gravity = player.gravityModifier;
            debugSet.SlamGravity = player.SlamSpeed;
            debugSet.DefaultSpeed = player.DefaultSpeed;
            debugSet.PlusSecondTierSpeed = player.PlusSecondTierSpeed;
            debugSet.PlusThirdTierSpeed = player.PlusThirdTierSpeed;
            debugSet.CrouchSpeed = player.MaxCrouchSpeed;
            debugSet.SlidingBaseSpeed = player.SlidingBaseSpeed;
            debugSet.SlidingDeceleration = player.SlidingDeceleration;
            debugSet.JumpHigh = player.JumpTakeOffSpeed;
            debugSet.TierChangeTime = player.RunTierChangeRunTime;
            debugSet.TurnBaseSpeed = player.BaseSpeed;
            debugSet.TurnAcceleration = player.Acceleration;
            debugSet.TurnStopTime = player.TurnStopTime;
            debugSet.WallWalkSpeed = player.WallWalkSpeed;
            debugSet.WallJumpSpeed = player.WallJumpSpeed;

            string json = JsonUtility.ToJson(debugSet);
            File.WriteAllText(settingsFilePath, json);
        }

        public void LoadDebugSettings(PlayerController player)
        {
            if (File.Exists(settingsFilePath))
            {
                string json = File.ReadAllText(settingsFilePath);
                DebugSetting debugSet = JsonUtility.FromJson<DebugSetting>(json);

                player.gravityModifier = debugSet.Gravity;
                player.SlamSpeed = debugSet.SlamGravity;
                player.DefaultSpeed = debugSet.DefaultSpeed;
                player.PlusSecondTierSpeed = debugSet.PlusSecondTierSpeed;
                player.PlusThirdTierSpeed = debugSet.PlusThirdTierSpeed;
                player.MaxCrouchSpeed = debugSet.CrouchSpeed;
                player.SlidingBaseSpeed = debugSet.SlidingBaseSpeed;
                player.SlidingDeceleration = debugSet.SlidingDeceleration;
                player.JumpTakeOffSpeed = debugSet.JumpHigh;
                player.RunTierChangeRunTime = debugSet.TierChangeTime;
                player.BaseSpeed = debugSet.TurnBaseSpeed;
                player.Acceleration = debugSet.TurnAcceleration;
                player.TurnStopTime = debugSet.TurnStopTime;
                player.WallWalkSpeed = debugSet.WallWalkSpeed;
                player.WallJumpSpeed = debugSet.WallJumpSpeed;
            }
            else
            {
                // Return default settings if file does not exist
                SaveDebugSettings(player);
            }
        }
    }

}

