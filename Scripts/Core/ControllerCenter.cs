using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Core
{
    public class ControllerCenter : MonoBehaviour, DefaultDefin
    {
        public static ControllerCenter Instance { get; private set; }
        [SerializeField]
        public List<GameObject> Controllers = new List<GameObject>();

        public enum ControllerList
        {
            GameController,
            MapController,

        }

        public void Init()
        {
            Instance = this;
            DontDestroyOnLoad(this);

            // 자기 아래에 있는 모든 오브젝트를 넣는다.
            foreach(Transform controller in transform)
            {
                Controllers.Add(controller.gameObject);
            }
        }

        private void Awake()
        {
            Init();
        }

    }

}

