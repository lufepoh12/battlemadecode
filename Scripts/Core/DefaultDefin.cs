using System.Runtime.InteropServices;
using UnityEngine;

namespace Platformer.Core
{
    /// <summary>
    /// 몬스터인지 구분하기 만듬
    /// </summary>
    public interface DefaultDefin
    {
        public void Init();
    }

}