using Cinemachine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.UI
{
    public class CameraShake : MonoBehaviour
    {
        public CinemachineVirtualCamera virtualCamera;
        public float shakeDuration = 0.1f;
        public float shakeAmplitude = 1f;
        public float shakeFrequency = 1f;

        private CinemachineBasicMultiChannelPerlin perlin;

        private void Start()
        {
            perlin = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        private void Update()
        {
            //if (Input.GetKeyDown(KeyCode.X))
            //{
            //    StartShake();
            //}
        }

        public void StartShake()
        {
            perlin.m_AmplitudeGain = shakeAmplitude;
            perlin.m_FrequencyGain = shakeFrequency;
            Invoke(nameof(StopShake), shakeDuration);
        }

        private void StopShake()
        {
            perlin.m_AmplitudeGain = 0f;
            perlin.m_FrequencyGain = 0f;
        }
    }

}

