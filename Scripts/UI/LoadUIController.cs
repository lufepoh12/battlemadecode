using Platformer.Core;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Platformer.UI
{
    public class LoadUIController : MonoBehaviour, DefaultDefin
    {
        public const string LoadUIControllerName = "LoadingCanvas";

        public GameObject LoadingImg;
        public float FadeDuration = 1f;
        public CanvasGroup MyCanvasGroup;

        private AsyncOperation ao;

        private bool isCoroutineFinished = false;

        public void Init()
        {
            if (LoadingImg == null)
            {
                LoadingImg = GameObject.Find("LoadingImg");
            }

            MyCanvasGroup = GetComponent<CanvasGroup>();

            gameObject.name = LoadUIControllerName;
        }

        private void Awake()
        {
            Init();            
        }

        public async Task FadeIn(float time = 1f)
        {
            isCoroutineFinished = false;

            StartCoroutine(FadeImageIn(time));

            await Task.Run(() =>
            {
                while (!isCoroutineFinished)
                {
                    Task.Yield();
                }
            });
        }

        private IEnumerator FadeImageIn(float time = 1f)
        {
            float elapsedTime = 0f;
            if (time != 1f)
            {
                FadeDuration = time;
            }

            while (elapsedTime < FadeDuration)
            {
                MyCanvasGroup.alpha = Mathf.Lerp(0f, 1f, elapsedTime / FadeDuration);
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            MyCanvasGroup.alpha = 1f;

            isCoroutineFinished = true;
        }

        public async Task FadeOut(float time = 1f)
        {
            isCoroutineFinished = false;

            StartCoroutine(FadeImageOut(time));

            await Task.Run(() =>
            {
                while (!isCoroutineFinished)
                {
                    Task.Yield();
                }
            });
        }
        private IEnumerator FadeImageOut(float time = 1f)
        {
            float elapsedTime = 0f;
            if (time != 1f)
            {
                FadeDuration = time;
            }

            while (elapsedTime < FadeDuration)
            {
                MyCanvasGroup.alpha = Mathf.Lerp(1f, 0f, elapsedTime / FadeDuration);
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            MyCanvasGroup.alpha = 0f;

            isCoroutineFinished = true;
        }

        

        public void LoadSceneAsyncLogic(string nextSceneName, string currentSceneName)
        {
            LoadSceneAsyncLogic(nextSceneName, currentSceneName, null);
        }
        public async void LoadSceneAsyncLogic(string nextSceneName, string currentSceneName, Action loadEndAction)
        {
            this.gameObject.SetActive(true);

            await FadeIn();

            ao = SceneManager.LoadSceneAsync(nextSceneName, LoadSceneMode.Additive);

            while (!ao.isDone)
            {
                await Task.Yield();
            }

            Scene nextScene = SceneManager.GetSceneByName(nextSceneName);
            SceneManager.MoveGameObjectToScene(this.gameObject, nextScene);
            var cutScene = GameObject.Find("CutSceneCanvas");
            SceneManager.MoveGameObjectToScene(cutScene, nextScene);
            var setScene = GameObject.Find("SettingCanvas");
            SceneManager.MoveGameObjectToScene(setScene, nextScene);
            SceneManager.SetActiveScene(nextScene);

            ao = SceneManager.UnloadSceneAsync(currentSceneName);

            while (!ao.isDone)
            {
                await Task.Yield();
            }

            if (loadEndAction != null)
            {
                loadEndAction();
            }

            await FadeOut();

            this.gameObject.SetActive(false);
        }

        //private IEnumerator LoadGame(string nextSceneName, string currentSceneName)
        //{
        //    while (!ao.isDone)
        //    {
        //        yield return null;
        //    }

        //    yield return new WaitForSeconds(1f);

        //    Scene nextScene = SceneManager.GetSceneByName(nextSceneName);
        //    SceneManager.MoveGameObjectToScene(this.gameObject, nextScene);
        //    SceneManager.SetActiveScene(nextScene);

        //    ao = SceneManager.UnloadSceneAsync(currentSceneName);

        //    while (!ao.isDone)
        //    {
        //        yield return null;
        //    }
        //}


        public async void LoadActionAsyncLogic(Action action, float fadetime = 1f)
        {
            this.gameObject.SetActive(true);
            await FadeIn(fadetime);
            action();
            await FadeOut(fadetime);
            this.gameObject.SetActive(false);
        }
    }
}