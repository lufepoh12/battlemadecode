using Platformer.Core;

using System.Collections;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Platformer.UI
{
    public class StartMenuUIController : MonoBehaviour, DefaultDefin
    {
        public Button GameStartBtn;
        public Button SettingBtn;

        public LoadUIController LoadUIController;
        public VolumeSlider VolumeSlider;

        public GameObject Mask;
        public GameObject GrabMask;

        private bool gameStart = false;

        public void Init()
        {
            GameStartBtn = GameObject.Find("StartBtn").GetComponent<Button>();
            //GameStartBtn.onClick.AddListener(OnGameStartClick);

            SettingBtn = GameObject.Find("SettingBtn").GetComponent<Button>();

            LoadUIController = GameObject.Find("LoadingCanvas").GetComponent<LoadUIController>();
            LoadUIController.gameObject.SetActive(false);

            gameStart = false;
        }

        private void Awake()
        {
            Init();
        }

        private async void Update()
        {
            if(Input.GetKeyDown(KeyCode.Return))
            {
                if (!gameStart)
                {
                    gameStart = true;
                    //Debug.Log("Enter key is pressed.");

                    Mask.SetActive(false);
                    GrabMask.SetActive(true);

                    await Task.Delay(1000);

                    //AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
                    // 원하는 애니메이션까지 대기
                    //while (!stateInfo.IsName(stateName))
                    //while (stateInfo.speed == )
                    //{
                    //    //전환 중일 때 실행되는 부분
                    //    await Task.Yield();
                    //}

                    //while (stateInfo.normalizedTime < 1.0f)
                    //{
                    //    await Task.Yield();

                    //    //애니메이션 재생 중 실행되는 부분
                    //    Debug.Log("asd");
                    //}
                    //Debug.Log("End");
                    OnGameStartClick();
                }
                
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                if (!gameStart)
                {
                    //Debug.Log("X key is pressed.");
#if UNITY_EDITOR
                    EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
                }
            }
        }

        private async void OnGameStartClick()
        {
            await GameObject.Find("CutSceneCanvas").GetComponent<CutSceneManager>()
                .OnCutScene(CutSceneManager.CutSceneType.Intro);

            LoadUIController.LoadSceneAsyncLogic("GameScene", "MainMenuScene");
        }
    }

}