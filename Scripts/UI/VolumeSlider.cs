using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Platformer.UI
{
    public class VolumeSlider : MonoBehaviour
    {
        public const string VolumeSliderName = "SettingCanvas";

        public AudioMixer mixer;
        public Slider slider;

        public GameObject SettingObj;

        private const float MinDecibels = -40;
        private const float MaxDecibels = 5;

        void Start()
        {
            slider.value = PlayerPrefs.GetFloat("MusicVolume", 0.50f);

            SettingObj.SetActive(false);
        }

        void Update()
        {
            if (!CutSceneManager.CutScenePlay)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    //Debug.Log("ESC key is pressed.");
                    if (SettingObj.activeSelf)
                    {
                        Time.timeScale = 1f; // Resume the game
                        SettingObj.SetActive(false);
                    }
                    else
                    {
                        Time.timeScale = 0f; // Pause the game
                        SettingObj.SetActive(true);
                    }
                }
            }
            else
            {
                if (SettingObj.activeSelf)
                    SettingObj.SetActive(false);
            }
        }

        public void SetLevel(float sliderValue)
        {
            float decibels = MinDecibels + (MaxDecibels - MinDecibels) * sliderValue;

            // When the slider is at the minimum position, set the volume to -80db.
            if (Mathf.Approximately(sliderValue, 0))
            {
                decibels = MinDecibels;
            }

            mixer.SetFloat("Master", decibels);
            PlayerPrefs.SetFloat("MusicVolume", sliderValue);
        }
    }

}

