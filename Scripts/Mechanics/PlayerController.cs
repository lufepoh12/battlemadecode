using Cinemachine;

using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Model;
using Platformer.UI;

using System;
using UnityEngine;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    public class PlayerController : KinematicObject, DefaultDefin
    {
        public AudioClip JumpAudio;
        public AudioClip RespawnAudio;
        public AudioClip OuchAudio;

        /// <summary>
        /// 플레이어 최대 수평 속도
        /// </summary>
        public float DefaultSpeed = 5;

        /// <summary>
        /// 3단계 최종적 달리기 속도
        /// </summary>
        public float PlusThirdTierSpeed = 2;

        /// <summary>
        /// 두번째 달리기 속도
        /// </summary>
        public float PlusSecondTierSpeed = 1;

        /// <summary>
        /// 플레이어 기어가는 최대 속도
        /// </summary>
        public float MaxCrouchSpeed = 2;


        public float MaxCrouchSlidingSpeed = 0f;

        /// <summary>
        /// 점프 시작 시의 초기 점프 속도
        /// </summary>
        public float JumpTakeOffSpeed = 7;

        public Vector2 IdleColliderOffset = new Vector2(0, -0.17f);
        public Vector2 IdleColliderSize = new Vector2(0.3f, 0.5f);
        public Vector2 CrouchColliderOffset = new Vector2(0, -0.32f);
        public Vector2 CrouchColliderSize = new Vector2(0.3f, 0.25f);

        // 뛰고 있는 시간.
        public float RunTimer = 0;
        // 3단계로 진입하는 시간
        public float RunTierChangeRunTime = 1;

        // 플레이어 방향
        public enum PlayerDirType
        {
            Left,
            Right
        }
        public PlayerDirType PlayerDirState = PlayerDirType.Left;

        // 달리기
        public enum RunStateType
        {
            Idle,
            PrepareToRun,
            Turn,
            Run,
            Wall,
            Stop,
        }
        public RunStateType RunState = RunStateType.Idle;
        // 달리기 단계
        public enum RunTierType
        {
            One,
            Two,
            TurnTwo,
            Three,
            TurnThree,
        }
        public RunTierType RunTierState = RunTierType.One;

        // 벽충돌 구현
        public enum WallStateType
        {
            Idle,
            PrepareToWallBump,
            PrepareToWallJump,
            PrepareToWallWalk,
            Walk,
            Bump,
            Jump,
        }
        public WallStateType WallState = WallStateType.Idle;

        // 점프
        public enum JumpStateType
        {
            Grounded,
            PrepareToJump,
            PrepareToSlam,
            Slam,
            Jumping,
            InFlight,
            Landed
        }
        public JumpStateType JumpState = JumpStateType.Grounded;

        // 기는 자세
        public enum CrouchStateType
        {
            Idle,
            PrepareToCrouch,
            PrepareToSliding,
            Crouch,
            Sliding,
            Wakeup
        }
        public CrouchStateType CrouchState = CrouchStateType.Idle;

        public BoxCollider2D MyCollider2d;
        public AudioSource MyAudioSource;
        public Health MyHealth;
        public Rigidbody2D MyRigidbody2d;
        public PlayerPlusCollisionCheck MyTopCheck;
        public PlayerPlusCollisionCheck MyBotCheck;
        public PlayerShootTheGun MyShoot;
        public PlayerBossHeart MyHeart;

        // 시간 당 증가시키는 양
        public float Acceleration = 2f;
        // 기준이 되는 속도
        public float BaseSpeed = 1f;
        // 지금 속도
        public float CurrentSpeed;
        // 턴했을때 멈췄을 시간
        public float TurnStopTime = 1f;

        // 슬라이딩시 줄어드는 가속도
        public float SlidingDeceleration = 2f;
        // 현재 슬라이딩 속도
        public float CurrentSlidingSpeed = 0f;
        // 슬라이딩 시작 속도
        public float SlidingBaseSpeed = 4f;

        // 벽 올라가는 속도
        public float WallWalkSpeed = 3f;
        // 벽 점프 날라가는 속도
        public float WallJumpSpeed = 3f;

        // 엉찍시 내려가는 속도
        public float SlamSpeed = 5f;


        public bool ControlEnabled = true;
        public bool SubControlEnabled = false;

        public Bounds Bounds => MyCollider2d.bounds;

        // 엉찍 중인지
        public bool IsSlam;

        internal bool _stopJump;
        internal bool _jump;
        internal bool _stopCrouch;
        internal bool _crouch = true;
        internal bool _crouchSliding = true;
        internal bool _stopRun;
        internal bool _run;
        internal bool _runTurn;
        internal bool _wallPrepare;
        internal bool _wallBump;
        internal bool _wallJump;
        internal bool _wallWalk;
        internal bool _slam;
        private Vector2 _move;
        private SpriteRenderer _mySpriteRenderer;
        private float _turnPlayCheck = 0f;
        // 방금전까지 향하던 방향
        private PlayerDirType _pastPlayreDir;
        // 벽점프시 계산용으로 사용함.
        private float _wallJumpDummySpeed = 0f;
        // 엉찍시 게산용으로 사용함.
        private float _slamDummySpeed = 0f;
        // 중력 기억용
        private float _pastGravity;
        // 일어날 수 있는 상황이 되면 일어나라.
        private bool _waitWakeup = false;

        internal float _runTurnVelocityX = 0;

        internal Animator _myAnimator;

        readonly PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        // 턴했을때 멈춰있는 시간 내부에서 쓰는 값
        internal float _turnStopTimer = 0f;

        // 엘리베이터 진입 중
        internal bool InElebator = false;

        public void Init()
        {
            MyHealth = GetComponent<Health>();
            MyAudioSource = GetComponent<AudioSource>();
            MyCollider2d = GetComponent<BoxCollider2D>();
            MyRigidbody2d = GetComponent<Rigidbody2D>();
            MyShoot = GetComponent<PlayerShootTheGun>();
            if (MyHeart == null)
                MyHeart = GameObject.Find("Hearts").GetComponent<PlayerBossHeart>();
            foreach (Transform child in transform)
            {
                var cc = child.GetComponent<PlayerPlusCollisionCheck>();

                if (cc != null)
                {
                    if (cc.Direction == PlayerPlusCollisionCheck.AllDirectionsType.Top)
                        MyTopCheck = cc;
                    else if (cc.Direction == PlayerPlusCollisionCheck.AllDirectionsType.Bottom)
                        MyBotCheck = cc;
                }
            }
            _mySpriteRenderer = GetComponent<SpriteRenderer>();
            _myAnimator = GetComponent<Animator>();

            // 기본 상태 콜라이더 크기 설정
            SetIdleEditCollider();

            // 설정 계산
            // 가해지는 값
            Acceleration = (DefaultSpeed + PlusThirdTierSpeed) + 3;
            // 멈췄을때 시작할 값.
            BaseSpeed = Acceleration / 2;

            //JsonManager.Instance.LoadDebugSettings(this);
            _pastGravity = gravityModifier;
            //JsonManager.Instance.SaveDebugSettings(this);
        }

        private void Awake()
        {
            Init();
        }

        protected override void Update()
        {
            if (ControlEnabled)
            {
                //JsonManager.Instance.LoadDebugSettings(this);
                //_pastGravity = gravityModifier;

                // 가로 이동 
                _move.x = Input.GetAxis("Horizontal");
                // 방향 기록
                if (Math.Sign(_move.x) > 0)
                {
                    _pastPlayreDir = PlayerDirState;
                    PlayerDirState = PlayerDirType.Right;
                }
                else if (Math.Sign(_move.x) < 0)
                {
                    _pastPlayreDir = PlayerDirState;
                    PlayerDirState = PlayerDirType.Left;
                }

                // 달리기
                if (RunState == RunStateType.Idle && CrouchState == CrouchStateType.Idle && WallState == WallStateType.Idle && 
                    Input.GetButton("Run"))
                {
                    RunState = RunStateType.PrepareToRun;
                    RunTierState = RunTierType.Two;
                }
                else if (Input.GetButtonUp("Run"))
                {
                    Schedule<PlayerStopRun>().player = this;
                }

                if (IsGrounded)
                {
                    if (JumpState == JumpStateType.Grounded && WallState == WallStateType.Idle && Input.GetButtonDown("Jump"))
                        JumpState = JumpStateType.PrepareToJump;
                    else if (Input.GetButtonUp("Jump"))
                    {
                        Schedule<PlayerStopJump>().player = this;
                    }
                }
                

                if (Input.GetButton("Down"))
                {
                    if (CrouchState == CrouchStateType.Idle && !_run && IsGrounded)
                    {
                        Debug.Log("A");
                        CrouchState = CrouchStateType.PrepareToCrouch;
                    }
                    else if (CrouchState == CrouchStateType.Idle && RunTierState == RunTierType.Three
                        && IsGrounded)
                    {// 바닥에서 슬라이딩
                        Debug.Log("B");
                        //Debug.Log(IsGrounded);
                        CrouchState = CrouchStateType.PrepareToSliding;
                    }
                    else if (CrouchState == CrouchStateType.Idle && !IsGrounded && !_slam)
                    {// 공중에서 엉찍
                        //Debug.Log("C");
                        _slam = true;
                        JumpState = JumpStateType.PrepareToSlam;
                    }
                }
                else if (Input.GetButtonUp("Down"))
                {
                    // 일어났을때 
                    if (!MyTopCheck.IsTopCheck)
                    {
                        CrouchState = CrouchStateType.Wakeup;
                        Schedule<PlayerStopCrouch>().player = this;
                    }
                    else
                    {
                        _waitWakeup = true;
                    }
                }
                else if (_waitWakeup)
                {
                    if (!MyTopCheck.IsTopCheck)
                    {
                        CrouchState = CrouchStateType.Wakeup;
                        Schedule<PlayerStopCrouch>().player = this;
                        _waitWakeup = false;
                    }
                }

                // 문으로 들어가기
                // 임시로 비활성화
                //if (Input.GetButtonDown("Up") && !InElebator)
                //{
                //    Debug.Log("Up Click");
                //    Collider2D[] colliders = Physics2D.OverlapBoxAll(transform.position, GetComponent<BoxCollider2D>().size, 0);
                //    foreach (Collider2D collider in colliders)
                //    {
                //        if (collider.gameObject.tag == "Door")
                //        {
                //            // Do something
                //            Debug.Log(collider.gameObject.name);
                //            var a = collider.GetComponent<DoorController>();
                //            if (a != null)
                //            {
                //                InElebator = true;
                //                a.OnElebator(this);
                //                return;
                //            }

                //            //if (collider.bounds.Contains(MyCollider2d.bounds.min) && collider.bounds.Contains(MyCollider2d.bounds.max))
                //            //{
                //            //    Debug.Log("CHECK");
                //            //}
                //            //if (collider.IsTouching(MyCollider2d))
                //            //{
                //            //}
                //        }
                //    }
                //}

                // 총 쏘기
                if (Input.GetButtonDown("Shot") && !_slam)
                {
                    Debug.Log("총쏘기");
                    if (MyShoot.ReadyToShoot)
                    {
                        _myAnimator.SetTrigger("shoot");
                        model.virtualCamera.GetComponent<CameraShake>().StartShake();
                        MyShoot.OnShoot();
                    }
                }
            }
            else
            {
                if (!SubControlEnabled)
                {
                    _move.x = 0;
                }
                else
                {
                    _move.x = velocity.x;
                }
            }
            UpdateWallState();
            UpdateJumpState();
            UpdateCrouchState();
            UpdateRunState();
            base.Update();
        }

        #region State Update 함수들
        /// <summary>
        /// 점프 상태값 변경
        /// </summary>
        protected void UpdateJumpState()
        {
            _jump = false;
            switch (JumpState)
            {
                case JumpStateType.PrepareToJump:
                    JumpState = JumpStateType.Jumping;
                    _jump = true;
                    _stopJump =false;
                    break;
                case JumpStateType.PrepareToSlam:
                    JumpState = JumpStateType.Slam;
                    Schedule<PlayerStopRun>().player = this;
                    _slam = true;
                    _stopJump = false;
                    break;
                case JumpStateType.Slam:
                    if (IsGrounded)
                    {
                        JumpState = JumpStateType.Grounded;
                        _slam = false;
                        _slamDummySpeed = SlamSpeed;
                    }
                    break;
                case JumpStateType.Jumping:
                    if (!IsGrounded)
                    {
                        Schedule<PlayerJumped>().player = this;
                        JumpState = JumpStateType.Landed;
                    }
                    break;
                case JumpStateType.InFlight:
                    if (IsGrounded)
                    {
                        Schedule<PlayerLanded>().player = this;
                        JumpState = JumpStateType.Landed;
                    }
                    break;
                case JumpStateType.Landed:
                    JumpState = JumpStateType.Grounded;
                    break;
            }
        }

        /// <summary>
        /// 기어가는 상태값
        /// </summary>
        protected void UpdateCrouchState()
        {
            switch (CrouchState)
            {
                case CrouchStateType.PrepareToCrouch:
                    CrouchState = CrouchStateType.Crouch;
                    _crouch = false;
                    _crouchSliding = true;
                    _stopCrouch = false;
                    CurrentSlidingSpeed = 0f;
                    // 콜라이더 크기 변경
                    SetCrouchEditCollider();
                    break;
                case CrouchStateType.PrepareToSliding:
                    CrouchState = CrouchStateType.Sliding;
                    _crouch = true;
                    _crouchSliding = false;
                    _stopCrouch = false;
                    // 콜라이더 크기 변경
                    SetCrouchEditCollider();
                    // 현재 속도 저장
                    // 이 속도는 주기적으로 줄어듬.
                    MaxCrouchSlidingSpeed = Math.Abs(velocity.x);
                    break;
                case CrouchStateType.Crouch:
                    if (IsGrounded && _stopCrouch)
                    {
                        Schedule<PlayerCrouch>().player = this;
                        _crouch = true;
                        CrouchState = CrouchStateType.Wakeup;
                    }
                    break;
                case CrouchStateType.Sliding:
                    // 주기적으로 슬라이딩 속도 줄이기
                    //Debug.Log(MaxCrouchSlidingSpeed);
                    CurrentSlidingSpeed += SlidingDeceleration * Time.deltaTime;
                    CurrentSlidingSpeed = Mathf.Clamp(CurrentSlidingSpeed, SlidingBaseSpeed, MaxCrouchSlidingSpeed);

                    MaxCrouchSlidingSpeed -= Time.deltaTime * CurrentSlidingSpeed;

                    if (IsGrounded && _stopCrouch || MaxCrouchSlidingSpeed <= 1)
                    {
                        Schedule<PlayerCrouchSliding>().player = this;
                        _crouch = true;
                        CrouchState = CrouchStateType.PrepareToCrouch;
                        RunState = RunStateType.Stop; // 런으로 2번 들어감. 그거 수정해야함.
                    }
                    break;
                case CrouchStateType.Wakeup:
                    CrouchState = CrouchStateType.Idle;
                    SetIdleEditCollider();
                    _crouch = true;
                    _crouchSliding = true;
                    CurrentSlidingSpeed = 0f;
                    break;
            }
        }

        /// <summary>
        /// 뛰어가기 상태값
        /// </summary>
        protected void UpdateRunState()
        {
            switch(RunState)
            {
                case RunStateType.PrepareToRun:
                    RunState = RunStateType.Run;
                    _run = false;
                    _runTurn = false;
                    _stopRun = false;
                    break;

                case RunStateType.Turn:
                    float velX = Math.Abs(velocity.x);

                    if (!_run)
                    {
                        RunState = RunStateType.Stop;
                    }
                    else if (velX < _runTurnVelocityX)
                    {
                        // 턴을 하다가 멈추는지 체크.
                        if (velX <= 0)
                        {
                            _turnPlayCheck += velX;

                            if (_turnPlayCheck <= 0 && _turnStopTimer > 0.1)
                            {
                                RunState = RunStateType.Stop;
                            }
                        }

                        _runTurn = true;

                        _turnStopTimer += Time.deltaTime;
                    }
                    else
                    {
                        RunState = RunStateType.Run;

                        _runTurn = false;
                        _run = true;

                        if (RunTierState == RunTierType.TurnTwo)
                        {
                            RunTierState = RunTierType.Two;
                        }
                        else
                        {
                            RunTierState = RunTierType.Three;
                            RunTimer = RunTierChangeRunTime;
                        }

                        _turnStopTimer = 0f;
                    }
                    break;

                case RunStateType.Run:
                    _runTurn = false;
                    if ((velocity.x > 0 && Input.GetAxisRaw("Horizontal") < 0) ||
                        (velocity.x < 0 && Input.GetAxisRaw("Horizontal") > 0))
                    {
                        //Debug.Log("오른쪽에서 왼쪽으로");
                        RunState = RunStateType.Turn;

                        if (RunTierState == RunTierType.Two)
                            RunTierState = RunTierType.TurnTwo;
                        else if (RunTierState == RunTierType.Three)
                            RunTierState = RunTierType.TurnThree;

                        Schedule<PlayerRunTurn>().player = this;
                    }
                    else if (Math.Abs(_move.x) <= 0 || _stopRun)
                    {
                        RunState = RunStateType.Stop;
                    }
                    else
                    {
                        if (!IsGrounded || CrouchState == CrouchStateType.Sliding)
                        {
                            //RunTimer = 0f;Debug.Log("ASDASD");
                        }
                        else
                            Schedule<PlayerRun>().player = this;
                    }
                    break;

                case RunStateType.Wall:
                    // 먼저 행동을 멈추게 만듬.
                    //Schedule<PlayerStopRun>().player = this;
                    //Schedule<PlayerWallBump>().player = this;
                    break;

                case RunStateType.Stop:
                    RunState = RunStateType.Idle;
                    RunTierState = RunTierType.One;
                    Schedule<PlayerStopRun>().player = this;
                    break;
            }
        }

        protected void UpdateWallState()
        {
            switch(WallState)
            {
                case WallStateType.Idle:
                    _wallPrepare = false;
                    _wallBump = false;
                    _wallJump = false;
                    _wallWalk = false;
                    break;

                case WallStateType.PrepareToWallWalk: // 벽올라가기
                    WallState = WallStateType.Walk;
                    _wallPrepare = true;
                    _wallWalk = true;
                    _wallBump = false;
                    _wallJump = false;
                    break;

                case WallStateType.PrepareToWallBump:
                    WallState = WallStateType.Bump;
                    _wallPrepare = true;
                    _wallBump = true;
                    _wallJump = false;
                    _wallWalk = false;
                    break;

                case WallStateType.PrepareToWallJump:
                    WallState = WallStateType.Jump;
                    _wallPrepare = true;
                    _wallJump = true;
                    _wallBump = false;
                    _wallWalk = false;

                    //if (RunTierState != RunTierType.Three)
                    //{
                    //    WallState = WallStateType.Bump;
                    //}
                    //else
                    //{
                    //    WallState = WallStateType.Jump;
                    //}
                    break;

                case WallStateType.Walk:
                    if (_pastPlayreDir != PlayerDirState)
                    {
                        //WallState = WallStateType.PrepareToWallJump;
                        WallState = WallStateType.Jump;
                        _wallPrepare = true;
                        _wallJump = true;
                        _wallBump = false;
                        _wallWalk = false;
                    }
                    else
                    {
                        // 벽 올라가기
                        velocity.y = WallWalkSpeed;

                        if (MyTopCheck.IsTopCheck)
                        {
                            Debug.Log("headhurt");
                            _myAnimator.SetTrigger("headhurt");
                            WallState = WallStateType.Idle;
                        }
                    }
                    break;

                case WallStateType.Bump:
                    // 먼저 행동을 멈추게 만듬.
                    Schedule<PlayerStopRun>().player = this;
                    //Schedule<PlayerWallBump>().player = this;
                    break;

                case WallStateType.Jump:
                    // 먼저 행동을 멈추게 만듬.
                    Schedule<PlayerStopRun>().player = this;
                    _wallJumpDummySpeed = WallJumpSpeed;
                    //Schedule<PlayerWallJump>().player = this;

                    //if (velocity.y == 0)
                    //{
                    //    WallState = WallStateType.Idle;
                    //}
                    //else
                    //{
                    //    // 먼저 행동을 멈추게 만듬.
                    //    Schedule<PlayerStopRun>().player = this;
                    //    Schedule<PlayerWallJump>().player = this;
                    //}
                    break;
            }
        }

        protected void UpdateAttackState()
        {

        }

        #endregion

        protected override void ComputeVelocity()
        {
            // 엉찍 스피드 조절
            if (_slamDummySpeed > 0f && !IsGrounded)
            {
                gravityModifier = _slamDummySpeed;
            }
            else
            {
                gravityModifier = _pastGravity;
                _slamDummySpeed = 0f;
            }

            if (_jump && IsGrounded)
            {
                velocity.y = JumpTakeOffSpeed * model.jumpModifier;
                _jump = false;
            }
            else if (_stopJump)
            {
                _stopJump = false;
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * model.jumpDeceleration;
                }
            }

            if (_move.x > 0.01f)
                _mySpriteRenderer.flipX = true;
            else if (_move.x < -0.01f)
                _mySpriteRenderer.flipX = false;
            else
                CurrentSpeed = 0f;

            _myAnimator.SetBool("crouch", _crouch);
            _myAnimator.SetBool("crouchSliding", _crouchSliding);
            _myAnimator.SetBool("grounded", IsGrounded);
            _myAnimator.SetBool("fast", _run);
            _myAnimator.SetBool("turn", _runTurn);
            _myAnimator.SetFloat("velocityX", Mathf.Abs(velocity.x) / DefaultSpeed);
            // 정규화 하여 전달.
            _myAnimator.SetFloat("fastTime", GetNormalizedValue(RunTimer, RunTierChangeRunTime));
            _myAnimator.SetBool("wallbump", _wallBump);
            _myAnimator.SetBool("walljump", _wallJump);
            _myAnimator.SetBool("wallwalk", _wallWalk);
            _myAnimator.SetBool("wallPrepare", _wallPrepare);
            _myAnimator.SetBool("slam", _slam);

            if (!_crouch || !_crouchSliding)
            {
                if (!_crouch)
                    targetVelocity = _move * MaxCrouchSpeed;
                else if (!_crouchSliding)
                    targetVelocity = _move * MaxCrouchSlidingSpeed;
            }
            else if (_run)
            {
                if (_runTurn)
                {
                    if (TurnStopTime < _turnStopTimer)
                    {
                        // 시간이 갈수록 달리기 속도 증가
                        CurrentSpeed += Acceleration * Time.deltaTime;
                        CurrentSpeed = Mathf.Clamp(CurrentSpeed, BaseSpeed, DefaultSpeed + PlusThirdTierSpeed);

                        targetVelocity = _move * CurrentSpeed;
                    }
                    else
                    {
                        // 순간 반동.
                        targetVelocity = Vector2.Scale(_move, new Vector2(-1, -1));
                    }
                }
                else
                {
                    if (RunTimer >= RunTierChangeRunTime || RunTierState == RunTierType.Three)
                    {
                        // 3단계 달리기
                        targetVelocity = _move * (DefaultSpeed + PlusSecondTierSpeed + PlusThirdTierSpeed);

                        if (RunTierState != RunTierType.Three)
                            RunTierState = RunTierType.Three;
                    }
                    else if (RunTierState == RunTierType.Two)
                    {
                        // 2단계 달리기
                        targetVelocity = _move * (DefaultSpeed + PlusSecondTierSpeed);

                        if (RunTierState != RunTierType.Two)
                            RunTierState = RunTierType.Two;
                    }

                    //targetVelocity = _move * (MaxSpeed + PlusMaxSpeed);
                }
            }
            else
            {
                if (_wallJumpDummySpeed > 0f && !IsGrounded)
                {
                    // 벽점프 속도가 남아있다면
                    targetVelocity = _move * _wallJumpDummySpeed;
                    _wallJumpDummySpeed -= Time.deltaTime * 5;
                }
                else
                {
                    if (_wallJumpDummySpeed > 0f)
                        _wallJumpDummySpeed = 0f;

                    targetVelocity = _move * DefaultSpeed;
                }

                //targetVelocity = _move * MaxSpeed;
            }
        }

        private void SetIdleEditCollider()
        {
            MyCollider2d.offset = IdleColliderOffset;
            MyCollider2d.size = IdleColliderSize;
        }

        private void SetCrouchEditCollider()
        {
            MyCollider2d.offset = CrouchColliderOffset;
            MyCollider2d.size = CrouchColliderSize;
        }
    }
}


