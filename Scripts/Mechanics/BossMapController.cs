using JetBrains.Annotations;

using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Model;
using Platformer.UI;

using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    public class BossMapController : MonoBehaviour, DefaultDefin 
    {
        public CloseDoorPos CloseDoorPos;
        public GameObject CloseDoorTileMap;
        public AudioSource MyAudioSource;
        public BossController Boss;
        public PlayerBossHeart PlayerBossHeart;
        public Canvas MyCanvas;

        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public void Init()
        {
            CloseDoorPos = FindAnyObjectByType<CloseDoorPos>();
            CloseDoorPos.BossMap = this;

            MyAudioSource = GetComponent<AudioSource>();

            Boss = FindAnyObjectByType<BossController>();

            MyCanvas = model.cutSceneManager.GetComponent<Canvas>();

            PlayerBossHeart = GameObject.Find("Hearts").GetComponent<PlayerBossHeart>();
        }

        private void Awake()
        {
            Init();
        }

        public async void BossMapOpening()
        {
            // ������ ������ ����
            // ���ݱ�
            GameController.Instance.StopMusic();
            CloseDoorTileMap.SetActive(true);
            model.virtualCamera.GetComponent<CameraShake>().StartShake();
            // ������ �Ҹ�
            await Task.Delay(1000);
            await Task.Yield();
            // �ƾ� ���
            if (MyCanvas == null)
            {
                MyCanvas = FindAnyObjectByType<Canvas>();
            }
            MyCanvas.sortingOrder = 6;
            await model.cutSceneManager.OnCutScene(CutSceneManager.CutSceneType.BossStartCutScene);

            // ������ ȭ�� ��ȯ
            //model.virtualCamera.Follow = Boss.transform;
            //await Task.Delay(1000);
            //model.virtualCamera.Follow = model.player.transform;
            //await Task.Delay(500);
            GameController.Instance.ChangeMusic(await ResourcesManager.Instance.ResLoadAudioClipAsync(model.mapController.GetMapAudioData()));
            model.player.MyHeart.InitHearts(model.player.MyHealth.maxHP);
            model.player.ControlEnabled = true;
            if (Boss == null)
                Boss = FindAnyObjectByType<BossController>();
            Boss.OnStateMechanic();
        }
    }

}

