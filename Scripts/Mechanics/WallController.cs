using Platformer.Core;
using Platformer.Gameplay;

using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

using UnityEngine;
using UnityEngine.Tilemaps;

using static Platformer.Core.Simulation;
using static UnityEngine.ParticleSystem;

namespace Platformer.Mechanics
{
    public class WallController : MonoBehaviour , DefaultDefin
    {
        public enum WallType
        {
            IDLE, // 아무것도 아닌 상태.
            WALL, // 안 부서지는 벽
            BROKEN_WALL, // 부서지는 벽
        }

        public WallType MyTag;
        public Collider2D MyCollider2D;
        public TilemapCollider2D MyTilemapCollider2D;
        [SerializeField]
        public Tilemap MyTilemap;

        [SerializeField]
        // 부서지는 경우만 계산.
        public List<TileData> BrokenWalls = new List<TileData>();
        [Serializable]
        public class TileData
        {
            public Vector3Int pos;
            public TileBase tile;
            public GameObject tileObj;
        }

        private bool _wallTouch;
        private List<ContactPoint2D> contacts;
        private GridLayout gridLayout;

        public void Init()
        {
            MyCollider2D = GetComponent<Collider2D>();

            if (gameObject.tag == "Wall")
                MyTag = WallType.WALL;
            else if (gameObject.tag == "BrokenWall")
            {
                MyTag = WallType.BROKEN_WALL;

                MyTilemap = GetComponent<Tilemap>();
                MyTilemapCollider2D = MyTilemap.GetComponent<TilemapCollider2D>();
            }
            else
                MyTag = WallType.IDLE;
        }

        private void Awake()
        {
            Init();

            gridLayout = GetComponentInParent<GridLayout>();

            //if (MyTag == WallType.BROKEN_WALL)
            //{
            //    // Get the bounds of the collider
            //    Bounds bounds = MyCollider2D.bounds;

            //    // Get the position of the bottom-left corner of the bounds
            //    Vector3Int cellPosition = MyTilemap.WorldToCell(bounds.min);

            //    // Calculate the number of cells to check horizontally and vertically
            //    int xCells = Mathf.CeilToInt(bounds.size.x / MyTilemap.cellSize.x);
            //    int yCells = Mathf.CeilToInt(bounds.size.y / MyTilemap.cellSize.y);
            //    Debug.Log(cellPosition + "|" + xCells + " | " + yCells);
            //    // Loop through the cells
            //    for (int x = 0; x < xCells; x++)
            //    {
            //        for (int y = 0; y < yCells; y++)
            //        {
            //            // Calculate the position of the cell
            //            Vector3Int currentPosition = cellPosition + new Vector3Int(x, y, 0);

            //            // Check if the cell has a tile
            //            if (MyTilemap.HasTile(currentPosition))
            //            {
            //                // Do something with the collided cell
            //                Debug.Log("Collided with tile at position " + currentPosition);
            //                var a = MyTilemap.GetTile(currentPosition);
            //                TileData tileData = new TileData();
            //                tileData.pos = currentPosition;
            //                tileData.tile = a;
            //                BrokenWalls.Add(tileData);
            //                //MyTilemap.SetTile(currentPosition, null);
            //            }
            //        }
            //    }
            //}
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null)
            {
                if (MyTag == WallType.BROKEN_WALL)
                {
                    if (player.RunState == PlayerController.RunStateType.Run && 
                        player.RunTierState > PlayerController.RunTierType.One)
                    {
                        DestroyWall(collision);
                    }
                    //else if (player.MyBotCheck.IsBottomCheck)
                    //{
                    //    DestroyWall(collision);
                    //}
                    else
                    {
                        ContactPoint2D contact = collision.contacts[0];
                        Vector2 contactNormal = contact.normal;

                        float angle = Vector2.Angle(contactNormal, Vector2.up);
                        if (!Mathf.Approximately(angle, 180f)) // 위에 올라가면.
                        {
                            var ev = Schedule<PlayerWallCollision>();
                            ev.player = player;
                            ev.wallType = MyTag;

                            _wallTouch = true;
                        }
                    }
                }
                else if (MyTag == WallType.WALL)
                {
                    if (player.JumpState != PlayerController.JumpStateType.Slam &&
                    player.JumpState != PlayerController.JumpStateType.PrepareToSlam)
                    {
                        ContactPoint2D contact = collision.contacts[0];
                        Vector2 contactNormal = contact.normal;

                        float angle = Vector2.Angle(contactNormal, Vector2.up);
                        if (!Mathf.Approximately(angle, 180f)) // 위에 올라가면.
                        {
                            var ev = Schedule<PlayerWallCollision>();
                            ev.player = player;
                            ev.wallType = MyTag;

                            _wallTouch = true;
                        }
                    }
                }
            }
        }
 
        private void OnCollisionExit2D(Collision2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null)
            {
                var ev = Schedule<PlayerWallOut>();
                ev.player = player;

                _wallTouch = false;
            }
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            Debug.Log("Wall Stay : " + collision.gameObject.name);
            var colCheck = collision.gameObject.GetComponent<PlayerPlusCollisionCheck>();
            if (colCheck != null)
            {
                if (MyTag == WallType.BROKEN_WALL)
                {
                    if (colCheck.IsBottomCheck)
                    {
                        DestroyWall(collision);
                    }
                }
            }
            //var playerr = collision.gameObject.GetComponent<PlayerController>();
            //if (playerr != null)
            //{
            //    if (MyTag == WallType.BROKEN_WALL)
            //    {
            //        if (playerr.MyBotCheck.IsBottomCheck)
            //        {
            //            DestroyWall(collision);
            //        }
            //    }

            //}
            // 충돌이 발생했을때만.
            if (_wallTouch)
            {
                ContactPoint2D contact = collision.contacts[0];
                Vector2 contactNormal = contact.normal;

                float angle = Vector2.Angle(contactNormal, Vector2.up);

                if (Mathf.Approximately(angle, 180f)) // 위에 올라가면.
                {
                    var player = collision.gameObject.GetComponent<PlayerController>();
                    if (player != null)
                    {
                        var ev = Schedule<PlayerWallOut>();
                        ev.player = player;

                        _wallTouch = false;
                    }

                    Debug.Log(angle);
                    // Collision is occurring on top surface
                    // Do something here...
                }
            }
        }

        public void ColCheck(Collision2D collision)
        {
            DestroyWall(collision);
        }

        private void DestroyWall(Collision2D collision)
        {
            contacts = new List<ContactPoint2D>();
            // 충돌 콜리더 구함.
            collision.GetContacts(contacts);

            // NB: Bounds cannot have zero width in any dimension, including z
            var cellBounds = new BoundsInt(
                gridLayout.WorldToCell(MyTilemapCollider2D.bounds.min),
                gridLayout.WorldToCell(MyTilemapCollider2D.bounds.size) + new Vector3Int(0, 0, 1));

            foreach (var contact in contacts)
            {
                if (contact.collider != null)
                {
                    foreach (var cell in cellBounds.allPositionsWithin)
                    {
                        if (MyTilemap.HasTile(cell))
                        {
                            // Find closest world point to this cell's center within other collider
                            var cellWorldCenter = gridLayout.CellToWorld(cell);
                            var otherClosestPoint = MyTilemapCollider2D.ClosestPoint(cellWorldCenter);

                            float deltaX = Mathf.Abs(contact.point.x - otherClosestPoint.x);
                            float deltaY = Mathf.Abs(contact.point.y - otherClosestPoint.y);
                            float distance = Mathf.Sqrt(deltaX * deltaX + deltaY * deltaY);
                            //Debug.Log(contact.point + " : " + distance);
                            if (distance < 0.5)
                            {
                                var otherClosestCell = gridLayout.WorldToCell(otherClosestPoint);
                                MyTilemap.SetTile(otherClosestCell, null);
                            }
                        }
                    }
                }
            }
        }
    }
}
