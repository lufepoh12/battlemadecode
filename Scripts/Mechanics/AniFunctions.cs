using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Mechanics
{
    public class AniFunctions : MonoBehaviour
    {
        public PlayerController PlayerController;
        public KinematicObject KinematicObject;

        private void Awake()
        {
            PlayerController = GetComponent<PlayerController>();
            KinematicObject = GetComponent<KinematicObject>();
        }

        public void IsFrozen()
        {
            if (KinematicObject != null) 
            {
                KinematicObject.IsFrozen = true;
            }
        }

        public void IsUnFrozen()
        {
            if (KinematicObject != null)
            {
                KinematicObject.IsFrozen = false;
            }
        }

        public void IsReversal()
        {
            if (KinematicObject != null)
            {
                var sr = KinematicObject.gameObject.GetComponent<SpriteRenderer>();
                if (sr != null)
                {
                    sr.flipX = true;
                }
            }
        }

        public void IsUnReversal()
        {
            if (KinematicObject != null)
            {
                var sr = KinematicObject.gameObject.GetComponent<SpriteRenderer>();
                if (sr != null)
                {
                    sr.flipX = false;
                }
            }
        }

        public void IsSlam()
        {
            if (PlayerController != null)
            {
                PlayerController.IsSlam = true;
            }
        }

        public void IsUnSlam()
        {
            if (PlayerController != null)
            {
                PlayerController.IsSlam = false;
            }
        }

        // 이벤트 체크
        public void EventCheck()
        {
            Debug.Log("asd");
        }
    }

}

