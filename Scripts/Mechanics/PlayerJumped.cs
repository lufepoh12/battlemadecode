using Platformer.Core;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Mechanics
{
    public class PlayerJumped : Simulation.Event<PlayerJumped>
    {
        public PlayerController player;

        public override void Execute()
        {
            if (player.MyAudioSource && player.JumpAudio)
                player.MyAudioSource.PlayOneShot(player.JumpAudio);
        }
    }
}


