using Platformer.Core;
using Platformer.Model;
using Platformer.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

namespace Platformer.Mechanics
{
    public class DoorController : MonoBehaviour, DefaultDefin
    {
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        public Collider2D MyCollider2D;
        public int NextMapId = -1;
        public bool FakeDoor = false;
        public bool IsElebator = false;

        public AudioClip ElebatorOpenAudio;
        public AudioClip ElebatorBellAudio;

        private Transform center = null;

        public void Init()
        {
            MyCollider2D = GetComponent<Collider2D>();

            if (NextMapId == -1)
            {
                FakeDoor = true;
            }

            foreach (Transform a in transform)
            {
                center = a;
            }
        }

        private void Awake()
        {
            Init();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null && !FakeDoor)
            {
                if (MapController.Instance.CheckMap(Convert.ToByte(NextMapId)))
                {
                    if (!IsElebator)
                    {
                        Simulation.GetModel<PlatformerModel>().loadController.LoadActionAsyncLogic
                        (new Action(() => MapController.Instance.OpenMap(Convert.ToByte(NextMapId))), 0.2f);
                    }
                    //GameController.Instance.LoadUIController.LoadActionAsyncLogic(new Action(() => MapController.Instance.OpenMap(Convert.ToByte(NextMapId))), 0.2f);
                }
            }
        }

        public async void OnElebator(PlayerController player)
        {
            if (MapController.Instance.CheckMap(Convert.ToByte(NextMapId)))
            {
                // 엘리베이터 일 경우..
                if (IsElebator && center != null)
                {
                    MyCollider2D.enabled = false;
                    player.ControlEnabled = false;
                    player.SubControlEnabled = true;
                    await PlayerCenterMove(player);
                }
            }
        }

        private async Task PlayerCenterMove(PlayerController player)
        {
            while (true)
            {
                await Task.Yield();

                if (player.transform.position.x <= center.transform.position.x)
                {
                    player.velocity.x = 0.2f;
                }
                else
                {
                    player.velocity.x = -0.2f;
                }

                // 두 오브젝트 간의 거리 계산
                //double deltaX = Mathf.Abs(center.transform.position.x - player.transform.position.x);
                double deltaX = Mathf.Abs(player.transform.position.x - center.transform.position.x);
                if (deltaX < 0.2f)
                {
                    player.SubControlEnabled = false;
                    player.velocity.x = 0f;
                    player.MyAudioSource.clip = ElebatorBellAudio;
                    player.MyAudioSource.Play();
                    float time = 0f;
                    float clipLength = player.MyAudioSource.clip.length;
                    while(player.MyAudioSource.time < clipLength)
                    {
                        await Task.Yield();

                        time += Time.deltaTime;
                        if (clipLength < time)
                            break;
                    }
                    await Task.Delay(500);
                    player.MyAudioSource.PlayOneShot(ElebatorOpenAudio);
                    await Task.Delay(1000);

                    Simulation.GetModel<PlatformerModel>().loadController.LoadActionAsyncLogic
                        (new Action(() =>
                        {
                            MapController.Instance.OpenMap(Convert.ToByte(NextMapId));
                        }), 0.2f);

                    await model.cutSceneManager.OnCutScene(CutSceneManager.CutSceneType.ElebatorUp);

                    player.ControlEnabled = true;
                    player.InElebator = false;
                    break;
                }
            }
        }
    }

}

