using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Model;
using Platformer.UI;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


using static Platformer.Core.Simulation;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace Platformer.Mechanics
{
    public class GoldenBatController : BossController
    {
        public enum PatternType
        {
            Pattern1,
            Pattern2,
            Pattern3,
        }
        public PatternType PatternState;
        public ObjectSpawner Spawner;
        // 그림 오브젝트
        public GameObject PaintingPrefab;
        // 그림 파일
        public List<Sprite> PaintList;
        // 약점 유지 시간
        public float WeakHoldingTime = 6f;
        // 올라가는 속도
        public float UpSpeed = 10f;
        // 떨어지는 위치 과녁
        public GameObject DownTarget;

        // 현재 약점 시간
        private float currentWeakHoldingTIme = 0f;

        private GameObject currentPaintGo;
        // 약점상태
        private bool OnWeak = false;
        private Vector2 paintDestoryPos;

        private bool isColliding = false;

        private async void Start()
        {
            if (!IsOne)
            {
                IsOne = true;

                base.Init();

                Spawner = GetComponentInChildren<ObjectSpawner>();
                if (PaintingPrefab == null)
                {
                    PaintingPrefab = await ResourcesManager.Instance.ResLoadGameObjectAsync("Boss/Paint");
                }

                if(DownTarget == null)
                {
                    DownTarget = GameObject.Find("DropPos");
                }

                //OnStateMechanic();
            }
            else
            {
                Destroy(this);
            }
        }

        public override void PattenStop()
        {
            base.PattenStop();

            state = BossState.RestWeak;
        }

        internal override async void OnStateMechanic()
        {
            try
            {
                while (ControlEnabled)
                {
#if UNITY_EDITOR
                    if (EditorApplication.isPlaying == false)
                        break;
#endif

                    switch (state)
                    {
                        case BossState.Idle:
                            if (MyHealth.IsAlive)
                            {
                                if (base.cancellationTokenSource.IsCancellationRequested)
                                {
                                    cancellationTokenSource = new CancellationTokenSource();
                                }

                                Physics2D.IgnoreLayerCollision
                                    (LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Boss"), false);

                                animator.SetBool("IsHit", false);

                                // 패턴 실행
                                if (MyHealth.currentHP > Patten1Hp)
                                {
                                    animator.SetBool("IsJumping", false);
                                    PatternState = PatternType.Pattern1;
                                    state = BossState.Rushing;
                                    rb.constraints |= RigidbodyConstraints2D.FreezePositionY;
                                    rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                                }
                                else if (MyHealth.currentHP > Patten2Hp)
                                {
                                    animator.SetBool("IsRushing", false);
                                    PatternState = PatternType.Pattern2;
                                    state = BossState.Jumping;
                                    rb.constraints |= RigidbodyConstraints2D.FreezePositionY;
                                    rb.constraints = RigidbodyConstraints2D.FreezeRotation;
                                }
                            }
                            break;
                        case BossState.Rest:
                            // 그림 떨구기
                            PaintingPrefab.GetComponent<PaintController>().boss = this;
                            PaintingPrefab.GetComponent<SpriteRenderer>().sprite = PaintList[UnityEngine.Random.Range(0, PaintList.Count)];
                            Vector2 spriteSize = PaintingPrefab.GetComponent<SpriteRenderer>().sprite.bounds.size;
                            Vector3 localScale = PaintingPrefab.transform.localScale;
                            PaintingPrefab.GetComponent<BoxCollider2D>().size =
                                new Vector2(spriteSize.x / localScale.x, spriteSize.y / localScale.y);
                            Spawner.SpawnObject(PaintingPrefab);
                            state = BossState.Idle;
                            break;
                        case BossState.Rushing:
                            CancellationToken pattern1Cancell = base.cancellationTokenSource.Token;
                            try
                            {
                                // 돌진 실행
                                await Pattern1(pattern1Cancell);
                            }
                            catch (Exception)
                            {
                                Debug.Log("Pattern1 강제 종료");
#if UNITY_EDITOR
                                if (EditorApplication.isPlaying == false)
                                    break;
#endif
                            }
                            break;
                        case BossState.Jumping:
                            // 점프 실행
                            CancellationToken pattern2Cancll = base.cancellationTokenSource.Token;
                            try
                            {
                                // 돌진 실행
                                await Pattern2(pattern2Cancll);
                            }
                            catch (Exception)
                            {
                                Debug.Log("Pattern2 강제 종료");
#if UNITY_EDITOR
                                if (EditorApplication.isPlaying == false)
                                    break;
#endif
                            }
                            break;
                        case BossState.RestWeak:
                            // 약점 진입 중
                            switch (PatternState)
                            {
                                case PatternType.Pattern1:
                                    if (currentPaintGo == null)
                                    {
                                        currentPaintGo = GameObject.Find("DesPos");
                                        rb.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;

                                        Physics2D.IgnoreLayerCollision
                                        (LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Boss"), true);

                                        animator.SetBool("IsDash", false);
                                        animator.SetTrigger("IsPattern1Weak");
                                    }

                                    // 일단 그림이 부서진 위치로 빠르게 이동
                                    if (transform.position.x <= currentPaintGo.transform.position.x)
                                    {
                                        rb.velocity = new Vector2((RushSpeed * 2) * Vector2.right.x, transform.position.y);
                                    }
                                    else
                                    {
                                        rb.velocity = new Vector2((RushSpeed * 2) * Vector2.left.x, transform.position.y);
                                    }

                                    // 두 오브젝트 간의 거리 계산
                                    double deltaX = Mathf.Abs(transform.position.x - currentPaintGo.transform.position.x);
                                    if (deltaX < 0.3f)
                                    {
                                        rb.velocity = Vector2.zero;
                                        animator.SetBool("IsOhno", true);
                                        await Task.Delay(2000);
                                        rb.velocity = Vector2.zero;
                                        Destroy(currentPaintGo);
                                        currentPaintGo = null;
                                        state = BossState.Weak;
                                    }
                                    break;

                                case PatternType.Pattern2:
                                    if (currentPaintGo == null)
                                    {
                                        currentPaintGo = GameObject.Find("DesPos");
                                        rb.constraints = RigidbodyConstraints2D.FreezeRotation;

                                        Physics2D.IgnoreLayerCollision
                                        (LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Boss"), true);

                                        animator.SetBool("IsJumping", false);
                                        animator.SetBool("IsJumpStart", false);
                                        animator.SetBool("IsJump", false);
                                        animator.SetBool("IsDown", false);
                                        animator.SetBool("IsDownStart", false);
                                        animator.SetTrigger("IsPattern2Weak");

                                        rb.gravityScale = 1f;
                                    }
                                    // 그냥 낙하
                                    while (transform.position.y > DownTarget.transform.position.y)
                                    {
                                        double deltaY = Mathf.Abs(transform.position.y - DownTarget.transform.position.y);
                                        if (deltaY < 1)
                                        {
                                            animator.SetBool("IsWeakRun", true);
                                            break;
                                        }
                                        await Task.Yield();
                                    }

                                    while (true)
                                    {
                                        // 일단 그림이 부서진 위치로 빠르게 이동
                                        if (transform.position.x <= currentPaintGo.transform.position.x)
                                        {
                                            rb.velocity = new Vector2((RushSpeed * 2) * Vector2.right.x, transform.position.y);
                                        }
                                        else
                                        {
                                            rb.velocity = new Vector2((RushSpeed * 2) * Vector2.left.x, transform.position.y);
                                        }

                                        // 두 오브젝트 간의 거리 계산
                                        double pattern2DeltaX = Mathf.Abs(transform.position.x - currentPaintGo.transform.position.x);
                                        if (pattern2DeltaX < 0.2f)
                                        {
                                            rb.velocity = Vector2.zero;
                                            animator.SetBool("IsOhno", true);
                                            await Task.Delay(2000);
                                            rb.velocity = Vector2.zero;
                                            Destroy(currentPaintGo);
                                            currentPaintGo = null;
                                            state = BossState.Weak;
                                            break;
                                        }
                                        await Task.Yield();
                                    }
                                    break;
                                case PatternType.Pattern3:
                                    break;
                            }

                            break;
                        case BossState.Weak:
                            // 약점 상태
                            animator.SetBool("IsOhno", false);
                            animator.SetBool("IsScare", true);
                            currentWeakHoldingTIme += Time.deltaTime;
                            OnWeak = true;

                            if (WeakHoldingTime < currentWeakHoldingTIme)
                            {
                                currentWeakHoldingTIme = 0f;
                                OnWeak = false;
                                animator.SetBool("IsScare", false);
                                state = BossState.Idle;
                            }
                            break;
                        case BossState.Dead:
                            // 죽음 처리
                            model.player.ControlEnabled = false;
                            ControlEnabled = false;

                            GameController.Instance.StopMusic();

                            await model.cutSceneManager.OnCutScene(CutSceneManager.CutSceneType.GameWin);

                            // 임시로 게임 종료
#if UNITY_EDITOR
                            EditorApplication.isPlaying = false;
#else
                            Application.Quit();
#endif
                            break;
                    }

                    await Task.Yield();
                }

            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }
        
        internal override void OnCollisionEnter2D(Collision2D collision)
        {
            //if (OnWeak)
            //{

            //}
            //else
            //{
            //    if (IsColPlayerOrWall)
            //    {
            //        if (collision.gameObject.GetComponent<PlayerController>() && !isColliding)
            //        {
            //            isColliding = true;
            //            IsColPlayerOrWall = false;
            //            // 공격
            //            collision.gameObject.GetComponent<PlayerController>().MyHeart.Decrement();
            //            if (!collision.gameObject.GetComponent<PlayerController>().MyHeart.IsAlive)
            //            {
            //                // 죽음처리
            //            }
            //            model.virtualCamera.GetComponent<CameraShake>().StartShake();
            //            StartCoroutine(Colliding());
            //        }
            //        else if (collision.gameObject.GetComponent<WallController>())
            //        {
            //            IsColPlayerOrWall = false;
            //            model.virtualCamera.GetComponent<CameraShake>().StartShake();
            //        }
            //    }
            //}

        }

        public float jumpHeight = 1f;  // The height the character will jump.
        public float jumpTime = 0.5f;  // The duration of the jump in seconds.
        public float moveDistance = 1f;  // The distance the character will move sideways.
        private void OnCollisionStay2D(Collision2D collision)
        {
            if (OnWeak)
            {

            }
            else
            {
                if (IsColPlayer)
                {
                    if (collision.gameObject.GetComponent<PlayerController>())
                    {
                        isColliding = true;
                        IsColPlayer = false;
                        // 공격
                        Schedule<PlayerOuch>();
                        collision.gameObject.GetComponent<PlayerController>().MyHeart.Decrement();
                        if (!collision.gameObject.GetComponent<PlayerController>().MyHeart.IsAlive)
                        {
                            // 플레이어 죽음처리
                            ControlEnabled = false;
                            collision.gameObject.GetComponent<PlayerController>().ControlEnabled = false;

                            var sv = Schedule<PlayerDeath>();
                            sv.re = true;
                            sv.bossController = this;
                        }
                        model.virtualCamera.GetComponent<CameraShake>().StartShake();

                        if (PatternState == PatternType.Pattern2)
                        {
                            // 2번째 패턴이면 
                            // Apply a force upwards.
                            StartCoroutine(JumpAndMoveCoroutine());
                        }
                    }
                }

                if (IsColWall)
                {
                    if (collision.gameObject.GetComponent<WallController>())
                    {
                        IsColWall = false;
                        model.virtualCamera.GetComponent<CameraShake>().StartShake();
                    }
                }
            }
        }

        private IEnumerator JumpAndMoveCoroutine()
        {
            Vector3 startPos = transform.position;
            Vector3 endPos = startPos + new Vector3(moveDistance, jumpHeight, 0);

            float elapsedTime = 0;
            while (elapsedTime < jumpTime)
            {
                transform.position = Vector3.Lerp(startPos, endPos, elapsedTime / jumpTime);
                elapsedTime += Time.deltaTime;
                yield return null;
            }

            // Ensure the final position is accurate.
            transform.position = endPos;
        }

        //void OnCollisionExit2D(Collision2D collision)
        //{
        //    if (collision.gameObject.GetComponent<PlayerController>())
        //    {
        //        isColliding = false;
        //    }
        //}

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (OnWeak)
            {
                if (collision.gameObject.GetComponent<BulletCollision>())
                {
                    MyHealth.Decrement();
                    OnWeak = false;
                    animator.SetBool("IsHit", true);

                    if (MyHealth.IsAlive)
                    {
                        Debug.Log("약점 상태 : 총 맞음");
                        currentWeakHoldingTIme = WeakHoldingTime - 1f;
                    }
                    else
                    {
                        Debug.Log("죽음");
                        state = BossState.Dead;
                    }
                    
                }
            }
        }

        internal override async Task Pattern1(CancellationToken cancellationToken)
        {
            Debug.Log("Pattern1 시작");
            animator.SetBool("IsRushing", true);
            // 돌진 전 준비 자세
            await AniEndCheck("Boss-PrepareRushing", cancellationToken);
            cancellationToken.ThrowIfCancellationRequested();
            await Task.Delay(1000, cancellationToken);

            // 돌진
            IsColPlayer = true;
            IsColWall = true;
            if (!isColliding)
                isColliding = true;
            animator.SetBool("IsDash", true);
            await OnRushing(cancellationToken);
            cancellationToken.ThrowIfCancellationRequested();

            // 멈춘 후 휴식
            animator.SetBool("IsDash", false);
            animator.SetBool("IsRushing", false);
            await Task.Delay(RushDelay, cancellationToken);
            state = BossState.Rest;
            Debug.Log("Pattern1 끝");
        }

        private async Task OnRushing(CancellationToken cancellationToken)
        {
            // 벽이나 플레이어가 닿을 때까지 돌진 실행
            var a = GetPlayerDir();
            while (IsColPlayer && IsColWall)
            {
                // 돌진 실행
                rb.velocity = a * RushSpeed;

                cancellationToken.ThrowIfCancellationRequested();

                await Task.Yield();
            }

        }

        internal override async Task Pattern2(CancellationToken cancellationToken)
        {
            Debug.Log("Pattern2 시작");
            animator.SetBool("IsJumping", true);
            await Task.Delay(1000, cancellationToken);
            // 점프 실행
            IsColPlayer = true;

            animator.SetBool("IsJumpStart", true);
            var originalPos = transform.position;
            var bossPos = transform.position;
            var destPos = new Vector3(transform.position.x, transform.position.y + Spawner.GetSpawnerAverageY(), 0);
            while (destPos.y > bossPos.y)
            {
                bossPos = transform.position;
                rb.velocity = new Vector2(rb.velocity.x, UpSpeed);
                //rb.velocity = new Vector2(rb.velocity.x, UpSpeed);
                //Debug.Log("상승중 : " + transform.position.y);
                await Task.Yield();
                cancellationToken.ThrowIfCancellationRequested();
                animator.SetBool("IsJump", true);
            }
            // 올라간 위치에서 정지
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0f;
            await Task.Delay(1000);
            animator.SetBool("IsJumpStart", false);
            animator.SetBool("IsJump", false);
            await Task.Delay(500);
            // 낙하 위치 찾기
            var dropStartPos = Spawner.GetRandomSpawnerPos();
            transform.position = new Vector2(dropStartPos.x, transform.position.y);
            // 원생성
            animator.SetBool("IsDownStart", true);
            DownTarget.transform.position = new Vector2(dropStartPos.x, -1f);
            float counter = 0;
            Color spriteColor = DownTarget.GetComponent<SpriteRenderer>().color;
            // 원 크기 키우기
            while (counter < 1f)
            {
                counter += Time.deltaTime;
                float alpha = Mathf.Lerp(0, 1, counter / 1f);

                DownTarget.GetComponent<SpriteRenderer>().color = 
                    new Color(spriteColor.r, spriteColor.g, spriteColor.b, alpha);

                await Task.Yield();
                cancellationToken.ThrowIfCancellationRequested();
            }
            // 낙하
            animator.SetBool("IsDown", true);
            animator.SetBool("IsDownStart", false);
            rb.gravityScale = 1f;
            destPos = new Vector3(transform.position.x, transform.position.y, 0);
            while (originalPos.y < destPos.y)
            {
                double deltaY = Mathf.Abs(originalPos.y - transform.position.y);
                if (deltaY < 0.1)
                {
                    animator.SetBool("IsLanding", true);
                    animator.SetBool("IsDown", false);
                    model.virtualCamera.GetComponent<CameraShake>().StartShake();
                    DownTarget.GetComponent<SpriteRenderer>().color = spriteColor;
                    break;
                }
                rb.velocity = new Vector2(rb.velocity.x, -UpSpeed);
                await Task.Yield();
                cancellationToken.ThrowIfCancellationRequested();
                //Debug.Log("하강중 : " + transform.position.y + " | " + deltaY);
            }
            await Task.Delay(500, cancellationToken);
            animator.SetBool("IsLanding", false);
            // 휴식
            await Task.Delay(RushDelay, cancellationToken);
            state = BossState.Rest;
            Debug.Log("Pattern2 끝");
        }

        private void OnApplicationQuit()
        {
            // Cancel the task when the Unity editor exits Play mode
            cancellationTokenSource.Cancel();
        }

        private void OnDisable()
        {
            // Cancel the task when the GameObject is disabled
            cancellationTokenSource.Cancel();
        }
    }

}


