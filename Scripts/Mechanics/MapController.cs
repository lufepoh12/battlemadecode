using Cinemachine;

using Platformer.Core;
using Platformer.Model;

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;

using UnityEngine;
using UnityEngine.UI;

namespace Platformer.Mechanics
{
    public class MapController : MonoBehaviour, DefaultDefin
    {
        public static MapController Instance {  get; private set; }

        // �޸𸮻� �ε��� ��
        [SerializeField]
        public List<GameObject> MapDataList = new List<GameObject>();

        // ������ ��ġ�� ��
        public MapData PastMap = null; // ���� ��
        public MapData CurrentMap = null; // ���� ��
        public MapData NextMap = null; // ���� ��

        public string CurrentMusicName = string.Empty;

        public Image Background;
        public Sprite TutorialSpr;
        public Sprite BossSpr;

        public const string TUTORIAL_MUSIC_NAME = "Tutorial";
        public const string GOLDENBAT_MUSIC_NAME = "GoldBadBat";


        public void Init()
        {
            Instance = this;

            DontDestroyOnLoad(this);

            // �� �ε�
            ResLoadMap();

            // Scene�� ���� �ִ��� üũ
            var sceneMap = FindFirstObjectByType<MapData>();
            if (sceneMap != null )
            {
                CurrentMap = sceneMap;
            }
            else
            {
                OpenMap(0);
            }
        }

        private void Awake()
        {
            Init();
        }

        private void ResLoadMap()
        {
            if (MapDataList.Count == 0)
            {
                MapDataList = ResourcesManager.Instance.ResLoadGameObjectsAsync("Map");
            }
        }

        private async void LoadMap(GameObject map)
        {
            if (map == null)
            {
                return;
            }

            map = Instantiate(map);
            DeleteMap();
            if (CurrentMap != null)
            {
                PastMap = CurrentMap;
            }
            CurrentMap = map.GetComponent<MapData>();

            if (CurrentMap.Type == MapData.MapType.Boss)
            {
                GameController.Instance.StopMusic();
                Background.sprite = BossSpr;
            }

            map.transform.position = Vector3.zero;

            var model = Simulation.GetModel<PlatformerModel>();
            if (model.virtualCamera != null)
            {
                model.confiner = model.virtualCamera.gameObject.GetComponent<CinemachineConfiner>();
            }
            else
            {
                var gb = GameObject.Find("CM vcam1");
                model.confiner = gb?.GetComponent<CinemachineConfiner>();
                model.virtualCamera = gb?.GetComponent<CinemachineVirtualCamera>();
            }
            model.confiner.m_BoundingShape2D = CurrentMap.CinemaConfiner;
            model.spawnPoint = map.GetComponent<MapData>().SpawnPoint.transform;
            model.player.Teleport(model.spawnPoint.position);

            // ���� ����
            
            if (CurrentMap.Type != MapData.MapType.Boss)
            {
                var musicResult = GetMapAudioData();
                if (musicResult != "")
                {
                    GameController.Instance.ChangeMusic
                    (await ResourcesManager.Instance.ResLoadAudioClipAsync(musicResult));
                }
            }
        }

        private void DeleteMap()
        {
            if (CurrentMap != null)
            {
                Destroy(CurrentMap.gameObject);
            }
        }


        public void OpenMap(byte id)
        {
            if (MapDataList.Count == 0)
                ResLoadMap();

            if (MapDataList.Count < id)
            {
                Debug.LogError("�߸��� Map Id");
            }

            var map = MapDataList.Find(x => x.GetComponent<MapData>().Id == id);
            if (map != null)
            {
                LoadMap(map);
            }
        }

        public bool CheckMap(byte id)
        {
            if (MapDataList.Count > 0)
            {
                var map = MapDataList.Find(x => x.GetComponent<MapData>().Id == id);
                
                if (map != null )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        //public bool OpenMap(byte id)
        //{
        //    if (MapDataList.Count == 0)
        //        ResLoadMap();

        //    if (MapDataList.Count < id)
        //    {
        //        Debug.LogError("�߸��� Map Id");
        //        return false;
        //    }

        //    if (CurrentMap != null)
        //    {
        //        PastMap = CurrentMap;
        //        CurrentMap = null;
        //    }

        //    var map = MapDataList.Find(x => x.GetComponent<MapData>().Id == id);
        //    if (map != null)
        //    {
        //        CurrentMap = map.GetComponent<MapData>();

        //        // ������ üũ
        //        if (MapDataList.Count < id + 1)
        //        {
        //            var nextMap = MapDataList.Find(x => x.GetComponent<MapData>().Id == id + 1);
        //            NextMap = nextMap.GetComponent<MapData>();
        //        }
        //    }
        //    else
        //    {
        //        Debug.LogError("�� ����.");
        //        return false;
        //    }

        //    return true;
        //}

        public MapData GetMap(byte id)
        {
            if (MapDataList.Count > 0)
            {
                var map = MapDataList.Find(x => x.GetComponent<MapData>().Id == id);
                return map.GetComponent<MapData>();
            }

            return null;
        }

        // �ʿ� ���� Ʋ������ϴ� ������ ����.
        public string GetMapAudioData()
        {
            // Ʃ�丮�� ����
            if (CurrentMap.Type == MapData.MapType.Idle)
            {
                if (CurrentMusicName == TUTORIAL_MUSIC_NAME)
                    return "";

                CurrentMusicName = TUTORIAL_MUSIC_NAME;
                return TUTORIAL_MUSIC_NAME;
            }
            else if (CurrentMap.Type == MapData.MapType.Boss) // ������
            {
                if (CurrentMusicName == GOLDENBAT_MUSIC_NAME)
                    return "";

                CurrentMusicName = GOLDENBAT_MUSIC_NAME;
                return GOLDENBAT_MUSIC_NAME;
            }

            return string.Empty;
        }
    }

}

