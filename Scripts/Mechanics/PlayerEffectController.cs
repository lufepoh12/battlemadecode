using Platformer.Core;
using Platformer.Mechanics;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class PlayerEffectController : MonoBehaviour, DefaultDefin
{
    public enum EffectType
    {
        Run,
        TwoRun,
        ThreeRun,
        Jump,
        JumpEnd,
    }
    public EffectType EffectState;

    public PlayerController Player;
    public SpriteRenderer MySpriteRenderer;

    [Serializable]
    public class Sprites
    {
        public EffectType type;
        public List<Sprite> list;
        public byte count;
    }

    public List<Sprites> AniSprites;

    public float ThresHoldSpeed = 10f;
    public float AfterImageDuration = 0.5f;
    public float WaitTime = 0.1f;

    private float timeSinceLastAfterimage;

    public void Init()
    {
        //if (transform.parent.GetComponent<PlayerController>() != null)
        //    Player = transform.parent.GetComponent<PlayerController>();
        MySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Awake()
    {
        Init();
    }

    private void Update()
    {
        if (Player != null)
        {
            if (Player.RunTierState == PlayerController.RunTierType.One) // 일반 달리기
            {
                EffectState = EffectType.Run;
                PlaySpriteAni(EffectState);
            }
            if (Player.RunTierState == PlayerController.RunTierType.Two ||
                Player.RunTierState == PlayerController.RunTierType.TurnTwo ||
                Player.RunTierState == PlayerController.RunTierType.TurnThree) // 2번째 달리기
            {
                EffectState = EffectType.TwoRun;
                PlaySpriteAni(EffectState);
            }
            if (Player.RunTierState == PlayerController.RunTierType.Three) // 3번째 달리기
            {
                EffectState = EffectType.ThreeRun;
                PlaySpriteAni(EffectState);
            }
            timeSinceLastAfterimage += Time.deltaTime;
        }
    }

    private void PlaySpriteAni(EffectType effect)
    {
        try
        {
            var data = AniSprites.Find(x => x.type == effect);
            if (data == null)
            {
                return;
            }

            Vector2 speed = Player.velocity;
            var normalSpeed = speed.normalized;

            if ((normalSpeed.x > 0.1f || normalSpeed.x < -0.1f) &&
                timeSinceLastAfterimage > WaitTime)
            {
                if (gameObject != null)
                {
                    GameObject afterimage = Instantiate(gameObject);
                    //afterimage.transform.parent = this.transform;
                    afterimage.GetComponent<PlayerEffectController>().enabled = false;
                    afterimage.transform.position = (Vector2)transform.position - speed * Time.deltaTime;
                    if (data.count >= data.list.Count)
                    {
                        data.count = 0;
                    }
                    afterimage.GetComponent<SpriteRenderer>().sprite = data.list[data.count];
                    data.count++;

                    if (normalSpeed.x > 0f)
                        afterimage.GetComponent<SpriteRenderer>().flipX = true;
                    else if (normalSpeed.x < 0f)
                        afterimage.GetComponent<SpriteRenderer>().flipX = false;

                    afterimage.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);

                    Destroy(afterimage, AfterImageDuration);

                    timeSinceLastAfterimage = 0f;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }
}
