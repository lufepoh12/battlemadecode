﻿using System.Collections;
using System.Collections.Generic;
using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Model;
using UnityEngine;

namespace Platformer.Mechanics
{
    /// <summary>
    /// AnimationController integrates physics and animation. It is generally used for simple enemy animation.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer), typeof(Animator))]
    public class AnimationController : KinematicObject
    {
        /// <summary>
        /// Max horizontal speed.
        /// </summary>
        public float maxSpeed = 7;
        /// <summary>
        /// Max jump velocity
        /// </summary>
        public float jumpTakeOffSpeed = 7;

        /// <summary>
        /// Used to indicated desired direction of travel.
        /// </summary>
        public Vector2 move;

        /// <summary>
        /// Set to true to initiate a jump.
        /// </summary>
        public bool jump;

        /// <summary>
        /// Set to true to set the current jump velocity to zero.
        /// </summary>
        public bool stopJump;

        public EnemyRadar EnemyRadar;

        SpriteRenderer spriteRenderer;
        internal Animator animator;
        PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        protected virtual void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            animator = GetComponent<Animator>();

            EnemyRadar = GetComponentInChildren<EnemyRadar>();
        }

        protected override void ComputeVelocity()
        {
            if (jump && IsGrounded)
            {
                velocity.y = jumpTakeOffSpeed * model.jumpModifier;
                jump = false;
            }
            else if (stopJump)
            {
                stopJump = false;
                if (velocity.y > 0)
                {
                    velocity.y = velocity.y * model.jumpDeceleration;
                }
            }

            if (EnemyRadar != null)
            {
                if (EnemyRadar.OnRadarPlayer)
                    move.x = 0f;
            }

            if (EnemyRadar.OnRadarPlayer)
            {
                move.y = 0f;

                if (EnemyRadar.Direction)
                    spriteRenderer.flipX = false;
                else if (!EnemyRadar.Direction)
                    spriteRenderer.flipX = true;
            }
            else
            {
                if (move.x > 0.01f)
                    spriteRenderer.flipX = false;
                else if (move.x < -0.01f)
                    spriteRenderer.flipX = true;
            }

            

            animator.SetBool("grounded", IsGrounded);
            animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
            if (EnemyRadar != null)
            {
                animator.SetBool("scare", EnemyRadar.OnRadarPlayer);
                if (!EnemyRadar.OnRadarPlayer)
                {
                    targetVelocity = move * maxSpeed;
                }
            }
            else
            {
                targetVelocity = move * maxSpeed;
            }
        }
    }
}