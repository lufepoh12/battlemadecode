using Platformer.Core;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Platformer.Mechanics
{
    public class MapData : MonoBehaviour
    {
        [SerializeField]
        public enum MapType
        {
            Idle,
            Boss,
        }
        public MapType Type;
        public byte Id;
        public string MapName;
        public GameObject Map;
        public GameObject SpawnPoint;
        public Collider2D CinemaConfiner;

        private void Awake()
        {
            Map = transform.gameObject;

            if (MapName == null)
                MapName = Map.name;
        }
    }

}

