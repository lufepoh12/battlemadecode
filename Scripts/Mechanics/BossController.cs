using Platformer.Core;
using Platformer.Model;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

namespace Platformer.Mechanics
{
    public abstract class BossController : MonoBehaviour, DefaultDefin
    {
        internal PlatformerModel model = Simulation.GetModel<PlatformerModel>();
        public bool ControlEnabled = true;
        public static bool IsOne = false;

        public enum BossState
        {
            Idle,
            Rest,
            Rushing,
            Jumping,
            RestWeak,
            Weak,
            Dead
        }
        public BossState state = BossState.Idle;
        internal BossState currentState;

        public Health MyHealth;
        public float RushSpeed = 7.0f;
        public int RushDelay = 1500;

        // 쫄따꾸
        public GameObject MinionPrefab;
        public byte Patten1Hp = 6;
        public byte Patten2Hp = 4;
        public byte Patten3Hp = 2;

        internal Rigidbody2D rb;
        internal Collider2D col;
        internal Animator animator;
        internal SpriteRenderer spriteRenderer;

        internal CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        internal bool IsColPlayer = false;
        internal bool IsColWall = false;

        public void Init()
        {
            MyHealth = GetComponent<Health>();
            rb = GetComponent<Rigidbody2D>();
            col = GetComponent<Collider2D>();
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public virtual void PattenStop()
        {
            cancellationTokenSource.Cancel();
        }

        /// <summary>
        /// 패턴 실행 하는 곳.
        /// </summary>
        internal abstract void OnStateMechanic();

        /// <summary>
        /// 충돌체크. 기본적으로 플레이어와 벽을 체크
        /// </summary>
        /// <param name="collision"></param>
        internal virtual void OnCollisionEnter2D(Collision2D collision)
        {
            if (IsColPlayer)
            {
                if (collision.gameObject.GetComponent<PlayerController>())
                {
                    IsColPlayer = false;
                    // 공격
                    collision.gameObject.GetComponent<PlayerController>().MyHeart.Decrement();
                    if (!collision.gameObject.GetComponent<PlayerController>().MyHeart.IsAlive)
                    {
                        // 죽음처리
                    }
                }
            }

            if (IsColWall)
            {
                if (collision.gameObject.GetComponent<WallController>())
                {
                    IsColWall = false;
                }
            }
        }

        //private void OnTriggerEnter2D(Collider2D collision)
        //{
        //    if (IsColPlayerOrWall)
        //    {
        //        if (collision.gameObject.GetComponent<PlayerController>())
        //        {
        //            IsColPlayerOrWall = false;
        //        }
        //        else if (collision.gameObject.GetComponent<WallController>())
        //        {
        //            IsColPlayerOrWall = false;
        //        }
        //    }
        //}

        internal abstract Task Pattern1(CancellationToken cancellationToken);

        internal abstract Task Pattern2(CancellationToken cancellationToken);

        public async Task<bool> AniEndCheck(string stateName, CancellationToken cancellationToken)
        {
            bool result = false;

            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            // 원하는 애니메이션까지 대기
            //while (!stateInfo.IsName(stateName))
            while(stateInfo.shortNameHash != Animator.StringToHash(stateName))
            {
                //전환 중일 때 실행되는 부분
                await Task.Yield();
                stateInfo = animator.GetCurrentAnimatorStateInfo(0);
                cancellationToken.ThrowIfCancellationRequested();
            }

            while (stateInfo.normalizedTime < 1.0f)
            {
                await Task.Yield();

                cancellationToken.ThrowIfCancellationRequested();

                //애니메이션 재생 중 실행되는 부분
                Debug.Log(stateName);
                result = true;
                break;
            }

            return result;
        }

        /// <summary>
        /// 플레이어의 방향으로 이미지 전환
        /// </summary>
        public Vector2 GetPlayerDir()
        {
            Vector2 playerPos = model.player.transform.position;

            Vector2 direction = (playerPos - (Vector2)transform.position).normalized;
            direction.y = transform.position.y;
            if (direction.x > 0)
            {
                spriteRenderer.flipX = true;
            }
            else
            {
                spriteRenderer.flipX = false;
            }

            return direction;
        }
    }

}

