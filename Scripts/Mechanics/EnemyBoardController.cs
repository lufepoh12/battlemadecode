using System.Collections;
using System.Collections.Generic;

using Platformer.Core;
using Platformer.Gameplay;
using Platformer.Mechanics;
using UnityEngine;
using static Platformer.Core.Simulation;

namespace Platformer.Mechanics
{
    public class EnemyBoardController : MonoBehaviour, EnemyInfo, DefaultDefin
    {
        [field: SerializeField]
        public EnemyInfo.EnemyState State { get; set; }

        [field: SerializeField]
        public string Name { get; set; }
        [field: SerializeField]
        public string Description { get; set; }
        public AudioClip Ouch;
        //public float KnockbackForce = 10f;
        public float speed = 5f;
        public float jumpHeight = 2f;
        public float jumpDuration = 1f;
        public int numPoints = 10;
        public float DieTime = 2f;

        internal BoxCollider2D _collider;
        internal AudioSource _audio;
        internal Animator _animator;
        
        private SpriteRenderer _spriteRender;
        private Rigidbody2D _rigidbody;

        private float _dieTimer = 0f;

        public Bounds Bounds => _collider.bounds;

        public float knockbackForce = 10f;  // 되받아치는 힘
        public float knockbackDuration = 0.5f;  // 날라가는 시간
        public float parabolaHeight = 2f; // 포물선 높이
        private bool isKnockedBack = false; // check if enemy is being knocked back
        private Vector2 knockbackDirection; // direction of knockback
        private float knockbackTimer = 0f; // timer for knockback effect

        public void Init()
        {
            _collider = GetComponent<BoxCollider2D>();
            _audio = GetComponent<AudioSource>();
            _spriteRender = GetComponent<SpriteRenderer>();
            _animator = GetComponent<Animator>();
            _rigidbody = GetComponent<Rigidbody2D>();

            State = EnemyInfo.EnemyState.LIVE;
        }

        private void Awake()
        {
            Init();
        }
        private void Update()
        {
            switch(State)
            {
                case EnemyInfo.EnemyState.LIVE:
                    _animator.SetBool("death", false);
                    break;

                case EnemyInfo.EnemyState.DIE:
                    _animator.SetBool("death", true);

                    if (isKnockedBack)
                    {
                        // apply knockback force in the direction of the knockbackDirection vector
                        _rigidbody.velocity = knockbackDirection * knockbackForce;

                        // increment the knockback timer
                        knockbackTimer += Time.deltaTime;

                        // calculate the parabolic arc and update the enemy's position
                        float t = knockbackTimer / knockbackDuration; // calculate the current time of the knockback effect
                        Vector2 parabola = ParabolicArc(_rigidbody.velocity, t, parabolaHeight); // calculate the position on the parabolic arc
                        transform.position += (Vector3)parabola * Time.deltaTime;

                        // check if the knockback effect is over
                        if (knockbackTimer >= knockbackDuration)
                        {
                            isKnockedBack = false;
                            _rigidbody.velocity = Vector2.zero;
                            knockbackTimer = 0f;
                        }
                    }

                    if (_dieTimer > DieTime)
                    {
                        var ev = Schedule<EnemyDelete>();
                        ev.enemy = this;
                    }

                    _dieTimer += Time.deltaTime;
                    break;
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var player = collision.gameObject.GetComponent<PlayerController>();
            if (player != null)
            {
                // calculate the direction of knockback
                Vector2 direction = collision.transform.position - transform.position;
                direction = direction.normalized;

                // apply knockback effect
                isKnockedBack = true;
                knockbackDirection = -direction; // enemy is knocked back in the opposite direction of the player

                var ev = Schedule<PlayerEnemyCollision>();
                ev.player = player;
                ev.enemyBoard = this;
            }
        }

        //public void EnemyDeath()
        //{
        //    Vector2 dir = transform.position - _targetCol.transform.position;
        //    _rigidbody.AddForce(dir.normalized * speed, ForceMode2D.Impulse);
        //    StartCoroutine(DrawParabolicCurve(dir));
        //}

        Vector2 ParabolicArc(Vector2 initialVelocity, float t, float height)
        {
            Vector2 gravity = Physics2D.gravity;
            Vector2 displacement = initialVelocity * t + 0.5f * gravity * t * t;
            displacement.y += height;
            return displacement;
        }
    }

}

