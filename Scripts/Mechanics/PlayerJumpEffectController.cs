using Platformer.Core;
using Platformer.Mechanics;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class PlayerJumpEffectController : MonoBehaviour, DefaultDefin
{
    public enum EffectType
    {
        Jump,
        JumpEnd,
    }
    public EffectType EffectState;

    public PlayerController Player;
    public SpriteRenderer MySpriteRenderer;

    [Serializable]
    public class Sprites
    {
        public EffectType type;
        public List<Sprite> list;
        public byte count;
    }

    public List<Sprites> AniSprites;

    public float ThresHoldSpeed = 10f;
    public float AfterImageDuration = 0.5f;
    public float WaitTime = 0.1f;

    private float timeSinceLastAfterimage;
    private GameObject clone;

    protected bool play = false;
    protected bool jump = false;
    protected Sprites playList;
    protected PlayerJumpEffectController original;

    public void Init()
    {
        //if (transform.parent.GetComponent<PlayerController>() != null)
        //    Player = transform.parent.GetComponent<PlayerController>();
        MySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Awake()
    {
        Init();
    }

    private void Update()
    {
        if (play)
        {
            play = false;
            StartCoroutine(Play());
        }
        else if (Player != null)
        {
            if (Player.JumpState == PlayerController.JumpStateType.PrepareToJump ||
                Player.JumpState == PlayerController.JumpStateType.Jumping)
            {
                EffectState = EffectType.Jump;
                PlaySpriteAni(EffectState);
                jump = true;
            }
            if (jump && clone == null && Player.velocity.y < 0.1f)
            {
                EffectState = EffectType.JumpEnd;
                PlaySpriteAni(EffectState);
                jump = false;
            }
        }

        timeSinceLastAfterimage += Time.deltaTime;
    }

    private void PlaySpriteAni(EffectType effect)
    {
        try
        {
            var data = AniSprites.Find(x => x.type == effect);
            if (data == null)
            {
                return;
            }

            if (gameObject != null)
            {
                clone = Instantiate(gameObject);
                //afterimage.transform.parent = this.transform;
                //afterimage.GetComponent<PlayerJumpEffectController>().enabled = false;
                clone.GetComponent<PlayerJumpEffectController>().Player = null;
                clone.GetComponent<PlayerJumpEffectController>().play = true;
                clone.GetComponent<PlayerJumpEffectController>().EffectState = effect;
                clone.GetComponent<PlayerJumpEffectController>().playList = data;
                clone.transform.position = (Vector2)transform.position;
                clone.GetComponent<PlayerJumpEffectController>().original = this;
                if (data.count >= data.list.Count)
                {
                    data.count = 0;
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
    }

    IEnumerator Play()
    {
        while (playList.count <= playList.list.Count)
        {
            yield return null;

            if (timeSinceLastAfterimage > WaitTime)
            {
                if (playList.count < playList.list.Count)
                {
                    timeSinceLastAfterimage = 0f;
                    MySpriteRenderer.sprite = playList.list[playList.count];
                    MySpriteRenderer.color = new Color(1f, 1f, 1f, 0.5f);
                    playList.count++;
                }
                else
                {
                    original.clone = null;
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
