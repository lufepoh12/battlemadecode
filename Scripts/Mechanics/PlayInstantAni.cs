using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Unity.VisualScripting;
using UnityEngine;

using static UnityEngine.UI.Image;

/// <summary>
/// This class allows an audio clip to be played during an animation state.
/// </summary>
public class PlayInstantAni : StateMachineBehaviour
{
    /// <summary>
    /// The point in normalized time where the clip should play.
    /// </summary>
    public float t = 0.5f;
    /// <summary>
    /// If greater than zero, the normalized time will be (normalizedTime % modulus).
    /// This is used to repeat the audio clip when the animation state loops.
    /// </summary>
    public float modulus = 0f;

    /// <summary>
    /// The audio clip to be played.
    /// </summary>
    public List<Sprite> sprites = new List<Sprite>();
    public float WaitTime = 0.1f;
    float last_t = -1f;
    public bool OnlyOne = true;
    private byte count = 0;
    private float timeSinceLastAfterimage;
    private bool End = true;

    override public async void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var nt = stateInfo.normalizedTime;
        if (modulus > 0f) nt %= modulus;
        if (nt >= t && last_t < t)
        {
            if (OnlyOne)
            {
                if (End)
                {
                    End = false;
                    await Play(animator);
                }
            }
            else
            {
                await Play(animator);
            }
            
        }
        last_t = nt;
    }

    private async Task Play(Animator animator)
    {
        GameObject gb = new GameObject();
        gb.AddComponent<SpriteRenderer>();
        gb.transform.position = animator.transform.position;
        timeSinceLastAfterimage = 0f;
        count = 0;

        while (count <= sprites.Count)
        {
            await Task.Yield();

            timeSinceLastAfterimage += Time.deltaTime;

            if (timeSinceLastAfterimage > WaitTime)
            {
                if (count < sprites.Count)
                {
                    gb.GetComponent<SpriteRenderer>().sprite = sprites[count];
                    timeSinceLastAfterimage = 0f;
                    gb.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.5f);
                    count++;
                }
                else
                {
                    Destroy(gb);
                    End = true;
                    break;
                }
            }
        }
    }
}

