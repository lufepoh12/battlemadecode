using Cinemachine;

using Platformer.Core;
using Platformer.Model;
using Platformer.UI;

using Unity.VisualScripting;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Platformer.Mechanics
{
    /// <summary>
    /// This class exposes the the game model in the inspector, and ticks the
    /// simulation.
    /// </summary> 
    public class GameController : MonoBehaviour, DefaultDefin
    {
        public static GameController Instance { get; private set; }

        //This model field is public and can be therefore be modified in the 
        //inspector.
        //The reference actually comes from the InstanceRegister, and is shared
        //through the simulation and events. Unity will deserialize over this
        //shared reference when the scene loads, allowing the model to be
        //conveniently configured inside the inspector.
        public PlatformerModel model = Simulation.GetModel<PlatformerModel>();

        private AudioSource audioSource;

        public void Init()
        {
            if (Instance == null)
                Instance = this;

            audioSource = GetComponent<AudioSource>();
        }

        public void Awake()
        {
            Init();
        }

        async void OnEnable()
        {
            model.player = GameObject.Find("Lilpa")?.GetComponent<PlayerController>();
            model.loadController = GameObject.Find(LoadUIController.LoadUIControllerName)?.GetComponent<LoadUIController>();
            if (model.loadController == null)
            {
                model.loadController = 
                    Instantiate(await ResourcesManager.Instance.ResLoadGameObjectAsync("UI/LoadingCanvas")).GetComponent<LoadUIController>();
            }

            model.mapController = FindAnyObjectByType<MapController>();

            model.cutSceneManager = GameObject.Find(CutSceneManager.CutSceneCanvasName)?.GetComponent<CutSceneManager>();
            if (model.cutSceneManager == null)
            {
                model.cutSceneManager = 
                    Instantiate(await ResourcesManager.Instance.ResLoadGameObjectAsync("UI/CutSceneCanvas")).GetComponent<CutSceneManager>();
            }

            model.volumeSlider = GameObject.Find(VolumeSlider.VolumeSliderName)?.GetComponent<VolumeSlider>();
            if (model.volumeSlider == null)
            {
                model.volumeSlider =
                    Instantiate(await ResourcesManager.Instance.ResLoadGameObjectAsync("UI/"+ VolumeSlider.VolumeSliderName)).GetComponent<VolumeSlider>();
            }
            //ChangeMusic(await ResourcesManager.Instance.ResLoadAudioClipAsync(model.mapController.GetMapAudioData()));

            //Confiner = GameObject.Find("CM vcam1")?.GetComponent<CinemachineConfiner>();

            //Player = GameObject.Find("Lilpa")?.GetComponent<PlayerController>();

            //LoadUIController = GameObject.Find(LoadUIController.LoadUIControllerName)?.GetComponent<LoadUIController>();
            //if (LoadUIController == null)
            //{
            //    LoadUIController = Instantiate(await ResourcesManager.Instance.ResLoadGameObjectAsync("UI/LoadingCanvas")).GetComponent<LoadUIController>();
            //}
        }

        void OnDisable()
        {
            if (Instance == this) Instance = null;
        }

        void Update()
        {
            if (Instance == this) Simulation.Tick();
        }

        public void ChangeMusic(AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }

        public void StopMusic()
        {
            audioSource.Stop();
        }
    }
}